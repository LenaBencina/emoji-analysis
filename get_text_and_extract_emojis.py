# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 14:42:51 2017

@author: Lena
"""

import os
import pickle
import re
import ujson #json
from collections import defaultdict
from collections import OrderedDict
import datetime
from timeit import default_timer as timer
#import codecs
#from joblib import Parallel, delayed
import multiprocessing as mp
import profile
#from win_unc import UncCredentials, UncDirectory, UncDirectoryConnection


#####################################################################################################

#directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis'
directory = '/Users/miha/Documents/Emoji/emoji-analysis/'
os.chdir(directory)

# REGEX
global emoji_official
global r 
#emoji_official = pickle.load(open(directory +'\\SavedVar\\emoji_official.p', "rb")) # load emoji official dictionary
emoji_official = pickle.load(open(directory +'/SavedVar/emoji_official.p', "rb")) # load emoji official dictionary
emojis = sorted(emoji_official.keys(), key=len, reverse=True) # reverse to catch multichar first
r = re.compile(u'(' + u'|'.join(re.escape(u) for u in emojis) + u')')
 
#####################################################################################################


# TESTING regex directly on JSON
#emoji_from_json = list()
#with open('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data\\2017-10-23\\2017-10-23-23', encoding='utf-8') as test: # open file = tweets for an hour
#        counter = 0    
#        for i in test: # for each tweet within an hour
#                #print(i)
#                counter += 1
#                j = i.split('\t')[1].strip()
#                #print(j)
#                t = json.loads(j)
#                text = t['text']
#                e = r.findall(text)
#                if len(e) > 0:
#                    emoji_from_json.extend(e)
#
#count1 = Counter(emoji_from_json)
#count1mc = count1.most_common()

#####################################################################################################

# validate list of files - dates or dates with hour
def validate(list_of_names,hour,tweets):
    form = '%Y-%m-%d'
    if hour:
        form = form + '-%H'
    if tweets:
        form = form + '-tweets'
    for name in list_of_names:
        try:
            datetime.datetime.strptime(name, form)
        except ValueError:
            raise ValueError("Incorrect data format!")
    
#####################################################################################################
# directory
            
global data_directory   
#data_directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data\\'
data_directory = '//KT-MARS/TwitterSampleDacq/'
 
  
#####################################################################################################
# GETTING TWEETS FROM ORIGINAL JSONs

def extract_one_file(file_index):
    with open(file_directories_old[file_index], encoding='utf-8') as file_hour, open(file_directories_new[file_index],'a', encoding='utf-8') as file_hour_tweet:
        os.listdir()
        first_line = True # boolean for checking if first tweet
        for line in file_hour: # for each line = each tweet
            tweet_json = line.split('\t')[1].strip() 
            temp = ujson.loads(tweet_json) # decode the JSON string into a Python dictionary containing raw strings
            text, lang = temp['text'], temp['lang']
            text = text.replace('\n','').replace('\r','') # remove \n and \r
            
            retweet = 'retweeted_status' in temp
            #print(re.sub('<[^<]+?>', '',temp['source'])) # get source
            
            if not first_line: # if not first line, add new line
                file_hour_tweet.write('\n')                 
            
            file_hour_tweet.write(lang + '\t' + str(retweet)[:1] + '\t' + text) # write to file
            first_line = False
    return

def extract_all_tweets(data_directory):
    
    folders = os.listdir(data_directory) # list all folders
    validate(folders, hour=False, tweets=False) # check if correct folders
    
    for folder in folders:
        # create new folder
        if not os.path.exists(data_directory + folder + '-tweets'):
            os.mkdir(data_directory + folder + '-tweets')
        
        # get file names
        files = os.listdir(data_directory + folder) # list all files
        files = [file for file in files if len(file)==13] # remove unuseful files = files with only ID
        validate(files, hour=True, tweets=False) # validate names for correct files
        
        global file_directories_old
        file_directories_old = [data_directory + folder + '\\' + file for file in files]

        global file_directories_new        
        file_directories_new = [data_directory + folder + '-tweets\\' + file + '-tweets' for file in files]
        
#        for file_index in list(range(0, 24)):
#            extract_one_file(file_index)
#        
        break
    return                

start = timer()
extract_all_tweets(data_directory)
end = timer()
print(end-start)




# testing regex on new files

#emoji = list()
#with open(folder + '-tweets\\' + file + '-tweets', encoding='utf-8') as test_hour:
#    for tweet in test_hour:
#        emoji_new = r.findall(tweet)
#        if len(emoji_new) > 0:
#            emoji.extend(emoji_new)
#
#
#count2 = Counter(emoji)
#count2mc = count2.most_common()


###########################################################################################
# EXTRACTING EMOJIS + COUNTING

def tweet_processing(line):
    # get tweet and language
    temp = line.split('\t')
    tweet, retweet, lang = temp[2], temp[1], temp[0]
    
    # extract and save emojis
    emoji_new = r.findall(tweet) # use regex to find all emojis
    tweet_counter['All'][lang][retweet] += 1

    if emoji_new: # if list is not empty = if emojis in tweet
        tweet_counter['Emoji'][lang][retweet] += 1
        for emoji in emoji_new:
            count_dict[emoji][lang][retweet] += 1
    return


def create_json_count(file_index):
    global count_dict
    count_dict = OrderedDict((key, defaultdict(lambda: defaultdict(lambda:0))) for key in emoji_official.keys())
    
    # open file for reading tweet and creating another for saving json   
    with open(file_directories[file_index], encoding='utf-8') as file_hour:

        for line in file_hour:
            tweet_processing(line)
        
        return count_dict


def process_one_day(folder):
    # create new folder
    if not os.path.exists(data_directory + folder + '-emojis'):
        os.mkdir(data_directory + folder + '-emojis')
    
    # get file names
    files = os.listdir(data_directory + folder) # list all files
    validate(files, hour=True, tweets=True) # validate names for correct files
    
    global file_directories
    file_directories = [data_directory + folder + '\\' + file for file in files]
    
    results = map(create_json_count, list(range(0, 24)))
    
    for result, file_directory in zip(results, file_directories): # write json to file
        with open(file_directory.replace('ts', 'ts-emojis'),'w', encoding='utf-8') as file_emoji:
            # write to json  
            ujson.dump(result, file_emoji) # result is count_dict     
    
    # write one day tweet counter
    with open(data_directory + 'GeneralCounter','a', encoding='utf-8') as file_counter:
        file_counter.write('\n' + folder.replace('-tweets', '') + '\t')
        ujson.dump(tweet_counter, file_counter)
        
    return
    

def count_emoji(data_directory):    
    
    # folders
    folders = os.listdir(data_directory) # list all folders
    folders = [folder for folder in folders if 'tweets' in folder]
    validate(folders, hour=False, tweets=True) # check if correct folders
    
    global tweet_counter
    tweet_counter = {key: defaultdict(lambda: defaultdict(lambda:0)) for key in ['All','Emoji']}
    
    for folder in folders:
        process_one_day(folder)
    
    return

start = timer()
count_emoji(data_directory)
#profile.run('count_emoji()')
end = timer()
print(end-start)




#test_dict = {key: defaultdict(lambda: defaultdict(lambda:0)) for key in emoji_official.keys()}
#test_dict = OrderedDict((key, defaultdict(lambda: defaultdict(lambda:0))) for key in emoji_official.keys())
#
#test_dict['😀']['en']['R'] += 1
#test_dict['😀']['slo']['O'] += 1
#test_dict['😉']['en']['O'] += 1
#
#
#for key,value in test_dict.items():
#    if value:
#       print(key,value)
#
#
## test opening json of emoji count
#with open('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data\\2017-10-21-tweets-emojis\\2017-10-21-00-tweets-emojis', encoding = 'utf-8') as outfile:
#    for i in outfile: 
#        test_json = ujson.loads(i)
#
## print non empty keys and check if it matches the emoji count
#for key,value in test_json.items():
#    if value:
#        print(key,value)

#test1 = test_json # 35.52482745595092


# https://wiki.python.org/moin/PythonSpeed/PerformanceTips  TIPS - check for counting 

