# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 15:22:20 2017

@author: Administrator
"""


import os
import pickle
import re
import ujson #json
from collections import defaultdict
from collections import OrderedDict
import datetime
from timeit import default_timer as timer
#import codecs
#from joblib import Parallel, delayed
#import multiprocessing as mp
#import profile


#####################################################################################################

#directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis'
directory = '/Users/miha/Documents/Emoji/emoji-analysis'
os.chdir(directory)

# REGEX
global emoji_official
global r 
#emoji_official = pickle.load(open(directory +'\\SavedVar\\emoji_official.p', "rb")) # load emoji official dictionary
emoji_official = pickle.load(open(directory +'/SavedVar/emoji_official.p', "rb")) # load emoji official dictionary

emojis = sorted(emoji_official.keys(), key=len, reverse=True) # reverse to catch multichar first
r = re.compile(u'(' + u'|'.join(re.escape(u) for u in emojis) + u')')

#####################################################################################################

# validate list of files - dates or dates with hour
def validate(list_of_names,hour):
    form = '%Y-%m-%d'
    if hour:
        form = form + '-%H'
    for name in list_of_names:
        try:
            datetime.datetime.strptime(name, form)
        except ValueError:
            raise ValueError("Incorrect data format!")

#####################################################################################################

# directory         
global data_directory   
#data_directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data\\'
data_directory = '//KT-MARS/TwitterSampleDacq/'
#saving_directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data\\'
saving_directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\CountsData\\'

  
#####################################################################################################

def new_format(folder_name): # check date for correct format of folder or line in file
    form = '%Y-%m-%d'
    if datetime.datetime.strptime(folder_name, form) > datetime.datetime.strptime('2017-02-03', form):
        return True


# EXTRACTING TWEETS + COUNTING EMOJIS

def count_emoji_tweet(tweet, lang, retweet):
    # extract and save emojis
    emoji_new = r.findall(tweet) # use regex to find all emojis
    tweet_counter['All'][lang][retweet] += 1

    if emoji_new: # if list is not empty = if emojis in tweet
        tweet_counter['Emoji'][lang][retweet] += 1
        for emoji in emoji_new:
            count_dict[emoji][lang][retweet] += 1
    return

def parse_tweet(line):
    # choose appropriate operation depending on the form of a file
    if new_format(current_folder):
        tweet_json = line.split('\t')[1].strip() 
    else:
        tweet_json = line.replace(u'\ufeff', '').strip()
        
    temp = ujson.loads(tweet_json) # decode the JSON string into a Python dictionary containing raw strings
    if 'lang' not in temp: # if tweet doesnt contain lang
        lang = 'NA'
        print('lang not in tweet')
    else:
        lang = temp['lang']
    tweet = temp['text']
    tweet = tweet.replace('\n','').replace('\r','') # remove \n and \r
    retweet = 'retweeted_status' in temp # check if original tweet or retweet
    return tweet, lang, str(retweet)[:1]

def create_json_count(file_index):
    
    global count_dict
    count_dict = OrderedDict((key, defaultdict(lambda: defaultdict(lambda:0))) for key in emoji_official.keys())
    
    with open(file_directories_old[file_index], encoding='utf-8') as file_hour:
        for line in file_hour: # for each line = each tweet
            tweet, lang, retweet = parse_tweet(line) # get tweet, language and info about retweet
            count_emoji_tweet(tweet, lang, retweet) # process one line = one tweet
        
        return count_dict


def process_one_day(folder):
    global current_folder
    current_folder = folder
    
    # create new folder
    if not os.path.exists(saving_directory + folder + '-emojis'):
        os.mkdir(saving_directory + folder + '-emojis')
    
    # get file names
    files = os.listdir(data_directory + folder) # list all files
    if new_format(folder): # if date > 2017-02-03 we have to delete files with only ID
        files = [file for file in files if len(file)==13] # remove unuseful files = files with only ID

    validate(files, hour=True) # validate names for correct files
    
    global file_directories_old
    file_directories_old = [data_directory + folder + '\\' + file for file in files] # directories of original data       
    file_indices = list(range(0, len(file_directories_old)))
    
#    results = map(create_json_count, file_indices) # for each file: create dict for emoji count
#    
#    file_directories_new = [saving_directory + folder + '-emojis\\' + file + '-emojis' for file in files] # directories for new (json) files       
#
#    for result, file_directory in zip(results, file_directories_new): # write json to file
#        with open(file_directory,'w', encoding='utf-8') as file_emoji:
#            # write to json  
#            ujson.dump(result, file_emoji) # result is count_dict     
#    
#    # write one day tweet counter
#    with open(saving_directory + 'GeneralCounter','a', encoding='utf-8') as file_counter:
#        file_counter.write('\n' + folder + '\t')
#        ujson.dump(tweet_counter, file_counter)
#        
    return
    

def count_main(data_directory):    
    # folders
    folders = os.listdir(data_directory) # list all folders
    folders = folders[207:]
    validate(folders, hour=False) # check if correct folders
    
    global tweet_counter
    tweet_counter = {key: defaultdict(lambda: defaultdict(lambda:0)) for key in ['All','Emoji']}
    
    for folder in folders:
        process_one_day(folder)
    
    return

start = timer()
#count_main(data_directory)
#profile.run('count_emoji()')
end = timer()
print(end-start)




import filecmp
new_file = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data\\2017-10-21-emojis\\2017-10-21-02-emojis'
old_file = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data_sample_2_phases\\2017-10-21-tweets-emojis\\2017-10-21-02-tweets-emojis'
filecmp.cmp(new_file, old_file)


new_counter = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data\\GeneralCounter'
old_counter = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Data_sample_2_phases\\GeneralCounter'
filecmp.cmp(new_counter, old_counter)


c = 0
for date in folders:
    if date == '2015-08-05':
        print(c)
    c = c + 1









