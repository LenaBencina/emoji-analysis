# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 15:34:37 2017

@author: Lena
"""

# In this file we create pandas data frames for a quick report

import pickle
import pandas as pd
from collections import Counter
import pycountry
import json
import matplotlib
matplotlib.style.use('ggplot')       # Use ggplot style plots*
import os

# load
os.chdir('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis')
directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\SavedVar\\'

emoji_all = pickle.load(open(directory + "emoji_all.p", "rb"))
counter_all_tweets = pickle.load(open(directory + "counter_all_tweets.p", "rb"))
emojis_per_tweet = pickle.load(open(directory + "emojis_per_tweet.p", "rb"))
language = pickle.load(open(directory + "language.p", "rb"))

emoji_official = pickle.load(open(directory + 'emoji_official.p', "rb")) # load emoji official dictionary
emoji_length = pickle.load(open(directory + "emoji_length.p", "rb")) # load emoji dict for length
###########################################################################################


#### Mini analysis ####

## 2. EMOJI COUNT
emoji_count = Counter(emoji_all) # count emojis 
ord_emoji_count = emoji_count.most_common() # ordered count

# prepare lists for pandas df
emojis, freq, names, length = [],[],[],[]
for pair in ord_emoji_count: 
    emoji = pair[0]
    emojis.append(emoji)
    freq.append(pair[1])
    name = emoji_official[emoji]
    names.append(name)
    length.append(emoji_length[name]) 
    
# save to df
emoji_df = pd.DataFrame({'Emoji': emojis, 'Frequency':freq , 'Official name':names, 'Number of code points':length})
pickle.dump(emoji_df, open(directory + "emoji_df.p", "wb")) # save emoji count df


## 3. SINGLE vs MULTI CODE POINT
multi_df = emoji_df.loc[emoji_df['Number of code points'] > 1]
pickle.dump(multi_df, open(directory + "multi_df.p", "wb")) # save second df


## 4. LANGUAGE
lang_count_all = dict()
lang_count_emoji = dict()

for lang in language.keys():
    positive = sum(x > 0 for x in language[lang])
    all = len(language[lang])
    lang_count_all[lang], lang_count_emoji[lang] = all, positive
     

lang_perc = Counter({k: round((lang_count_emoji.get(k,1)/lang_count_all.get(k,1))*100,2) for k in lang_count_all.keys() & lang_count_emoji}) # percentage
ord_lang_perc = lang_perc.most_common() # ordered

## language info to df
lang_df = pd.DataFrame([lang_count_emoji, lang_count_all, lang_perc]).T
lang_df.columns = ["Number of tweets including emojis", "Number of all tweets", "%"]
lang_df = lang_df.sort_values("Number of tweets including emojis", ascending=0) # sort

# create dictionary {iso code: language name} 
codes_list = list(lang_df.index) # get iso lang codes
codes_list.remove('und') # remove unofficial codes
codes_list.remove('in')
codes_list.remove('iw')
codes_list.remove('ckb')
lang_names = {code:pycountry.languages.get(alpha_2=(code.strip())).name for code in codes_list} # dict {code: get lang name for code}
lang_names.update({'und': 'undefined','in':'?','iw':'?','ckb':'?'}) # manually add unofficial codes

# add column of language names
names_pd = [lang_names[code] for code in lang_df.index] # list of names  
se = pd.Series(names_pd) # convert to series
lang_df['Language name'] = se.values # add series as column to df

# save data frame
pickle.dump(lang_df, open(directory + "lang_df.p", "wb"))


### Check distribution of Number of emojis per tweet
def NestedDictValues(d):
  for v in d.values():
    if isinstance(v, dict):
      yield from NestedDictValues(v)
    else:
      yield v
    
os.getcwd()    
with open(directory + 'emoji_per_tweet_json.txt', 'w') as outfile:
    json.dump(emojis_per_tweet, outfile)

with open(directory + 'language_json.txt', 'w') as outfile:
    json.dump(language, outfile)


list_emojis_per_tweet = pd.DataFrame({'Number of tweets including at least one emoji': []})





## 5. EMOJI WITH FACE 

face = emoji_df[emoji_df['Official name'].str.contains('face')]


## 1. GENERAL INFO
tweet_with_emoji_count = lang_df['Number of tweets including emojis'].sum() 
general = pd.DataFrame({'Number of tweets including at least one emoji': [tweet_with_emoji_count], 'Number of all tweets': [counter_all_tweets], '% of tweets including emojis': [str(round(tweet_with_emoji_count/counter_all_tweets*100,3))]})
general = general.T
pickle.dump(general, open(directory + "general.p", "wb"))

lens = emoji_length.values()
lens_count = Counter(lens).most_common()

multi_cp_count = sum([x[1] for x in lens_count[1:]])
single_cp_count = lens_count[0][1]
multi_cp_perc = multi_cp_count/(multi_cp_count+single_cp_count)*100

  