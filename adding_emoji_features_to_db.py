#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 16:55:54 2018

@author: Lena
"""
import os
import psycopg2
import pandas as pd
from collections import Counter

os.chdir('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\emoji-analysis')
from config import config

def fetchall(query):
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor()   
    cur.execute(query)
    tmp = cur.fetchall()
    cur.close()
    conn.close()
    return tmp


emoji_id_all = pd.DataFrame(fetchall("""SELECT emoji_id, name FROM emoji"""), columns = ['id', 'name'])



####### MODIFIERS INFO ########

# from official page







# checking all the existing modifiers
names = emoji_id_all['name']
emoji_names_modifiers = [name for name in names if ':' in name and 'keycap' not in name]
test = [name for name in names if ':' not in name]

emoji_names_simple = list(set([m.split(':')[0] for m in emoji_names_modifiers])) # unique names for emojis which can be modified
all_modifiers = Counter([name.split(':')[1] for name in emoji_names_modifiers if ':' in name])

