# -*- coding: utf-8 -*-
"""
Created on Thu Jan  4 13:45:51 2018

@author: Lena
"""

import psycopg2
import os 
import pickle

import pandas as pd 
import pycountry
from collections import Counter
from collections import OrderedDict

import matplotlib.pyplot as plt
import matplotlib

import pandas.io.sql as psql
from sqlalchemy import create_engine


from functools import reduce

from datetime import date

from operator import itemgetter # for sorting list of lists

from matplotlib.cbook import get_sample_data
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.patches as mpatches

import seaborn as sns # heatmap
from matplotlib.ticker import FuncFormatter # % on ticks
from matplotlib.patches import Patch # legend markers

# os.chdir('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\emoji-analysis')
os.chdir('/Users/miha/Documents/Emoji/emoji-analysis/')

from config import config

global emoji_to_name
global name_to_emoji
global emoji_to_code
global code_to_emoji
global name_to_length


####### POSTGRES CONNECTION

params = config()
conn = psycopg2.connect(**params)
#conn.close()

engine = create_engine('postgresql://postgres:postgres@172.20.0.150:5432/Emoji')


###################### PLOT SETTINGS #######################################################

#prop = fm.FontProperties(fname='/Users/miha/Downloads/AppleColorEmoji.ttf')
#font = {'fontname' : prop.get_name()}

font = {'family' : 'sans-serif',
        'serif' : 'Segoe UI Emoji',
        'weight' : 'normal',
        'size'   : 20}

matplotlib.rc('font', **font)
plt.style.use('seaborn')

sns.set(font='Segoe UI Emoji')


#############################################################################################

dict_dir = '/Users/miha/Documents/Emoji/emoji-analysis/Dictionaries/'
#dict_dir = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\emoji-analysis\\Dictionaries'

#old_var_dir = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\emoji-analysis\\SavedVar\\' # windows
#old_var_dir = '/Users/miha/Documents/Emoji/emoji-analysis/SavedVar/' # mac
#
#
#emoji_to_name = pickle.load(open(old_var_dir + 'emoji_official.p', "rb")) # load emoji official dictionary
#emoji_to_code = pickle.load(open(old_var_dir + 'emoji_code.p', "rb")) # load emoji code dictionary
#name_to_length = pickle.load(open(old_var_dir + 'emoji_length.p', "rb")) # load emoji length dictionary
#
#
##### create inverse dictionaries
#def invert_dict(d):
#    return dict([ (v, k) for k, v in d.items( ) ])
#
#
#name_to_emoji = invert_dict(emoji_to_name)
#code_to_emoji = invert_dict(emoji_to_code)
#
#
#
### code - name
#code_to_name = dict([ (code, emoji_to_name[emoji] ) for code, emoji in code_to_emoji.items()])
#name_to_code = invert_dict(code_to_name)

# SAVING DICTIONARIES
#pickle.dump(emoji_to_name, open(dict_dir + "emoji_to_name.p", "wb"))
#pickle.dump(name_to_emoji, open(dict_dir + "name_to_emoji.p", "wb"))
#pickle.dump(emoji_to_code, open(dict_dir + "emoji_to_code.p", "wb"))
#pickle.dump(code_to_emoji, open(dict_dir + "code_to_emoji.p", "wb"))
#pickle.dump(name_to_length, open(dict_dir + "name_to_length.p", "wb"))
#pickle.dump(code_to_name, open(dict_dir + "code_to_name.p", "wb"))
#pickle.dump(name_to_code, open(dict_dir + "name_to_code.p", "wb"))

# LOADING DICTIONARIES
emoji_to_name = pickle.load(open(dict_dir + "emoji_to_name.p", "rb"))
name_to_emoji = pickle.load(open(dict_dir + "name_to_emoji.p", "rb"))
emoji_to_code = pickle.load(open(dict_dir + "emoji_to_code.p", "rb"))
code_to_emoji = pickle.load(open(dict_dir + "code_to_emoji.p", "rb"))
name_to_length = pickle.load(open(dict_dir + "name_to_length.p", "rb"))
code_to_name = pickle.load(open(dict_dir + "code_to_name.p", "rb"))
name_to_code = pickle.load(open(dict_dir + "name_to_code.p", "rb"))


#############################################################################################

# aux. function: fetchall on specific query
def fetchall(query):
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor()   
    cur.execute(query)
    tmp = cur.fetchall()
    cur.close()
    conn.close()
    return tmp

def fetchone(query):
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor()   
    cur.execute(query)
    tmp = cur.fetchone()
    cur.close()
    conn.close()
    return tmp


#############################################################################################
# dir for saving objects
#saving_csv_dir = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\TablesForReport\\' # windows
#saving_dir = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\DataFrames\\' # windows
saving_dir = '/Users/miha/Documents/Emoji/emoji-analysis/DataFrames' # mac

############################### save emoji table from db to pandas #########################
emoji_info_table = psql.read_sql("SELECT * FROM emoji", conn)
############################################################################################



###### Frequency table: Emoji, Freq., Name, Number of code points

#def get_emoji_freq_df():
#    params = config()
#    conn = psycopg2.connect(**params)
#    cur = conn.cursor()   
#    
#    # check the frequency for each emoji
#    cur.execute("""SELECT emoji_id, SUM(emoji_freq) AS freq
#                 FROM emoji_count
#                 GROUP BY emoji_id""")
#    
#    rows = cur.fetchall()
#    names = [x[0] for x in cur.description]
#    emoji_freq = pd.DataFrame(rows, columns=names)
#    emoji_freq['emoji'] = emoji_freq['emoji_id'].map(code_to_emoji)
#    emoji_freq['name'] = emoji_freq['emoji'].map(emoji_to_name)
#    emoji_freq['length'] = emoji_freq['name'].map(name_to_length)
#    #emoji_freq = emoji_freq.drop('emoji_id', axis=1)
#    emoji_freq = emoji_freq.sort_values(by=['freq'], ascending=False)
#    emoji_freq['index'] = list(range(1,emoji_freq.shape[0]+1))
#    
#    # check which emojis are not included and add it to the df
#    missing_names = list(set(emoji_to_name.values()) - set(emoji_freq['name']))
#    freq = [0] * len(missing_names)
#    emoji_id = list(map(name_to_code.get, missing_names))
#    emoji = list(map(name_to_emoji.get, missing_names))
#    length = list(map(name_to_length.get, missing_names))
#    index = list(range(emoji_freq.shape[0]+1, emoji_freq.shape[0]+1+len(emoji)))
#    emoji_freq = emoji_freq.append(pd.DataFrame({'freq': freq, 'emoji':emoji ,'name':missing_names,
#                                                 'length':length, 'index':index, 'emoji_id':emoji_id }))
#    
#    cur.close()
#    conn.close()
#    return emoji_freq
#
#freq_table = get_emoji_freq_df()
#
### ADD FREQUENCT TABLE TO DB
#freq_table.to_sql(name='emoji_frequency', con=engine, index=False)
#
#
### CHECKING POPULARITY / COMPARING TO EMOJI TRACKER
#freq_table.loc[freq_table['name'] == 'heart suit']
#freq_table.loc[freq_table['name'] == 'recycling symbol']
#freq_table.loc[freq_table['name'] == 'unamused face']
#freq_table.loc[freq_table['name'] == 'two hearts']
#freq_table.loc[freq_table['name'] == 'face blowing a kiss']
#
### MISSING EMOJIS
#freq_table.loc[freq_table['freq'] == 0]


freq_table = psql.read_sql("""SELECT emoji, emoji.emoji_id, freq, index, emoji.length, emoji.name, standard
                              FROM emoji_frequency LEFT JOIN emoji ON (emoji_frequency.emoji_id = emoji.emoji_id)
                              ORDER BY freq DESC""", conn)


################################# PLOT 1: PLOTTING EMOJI FREQUENCY TABLE #######################################


color_list = sns.cubehelix_palette(8,light=1)
sns.set(font_scale=1.4, font='Segoe UI Emoji')

freq_table['name with emoji'] = freq_table['name'] + ' ' + freq_table['emoji']
freq_table_pop = freq_table.head(40) # 40 most popular
colors = {None: color_list[3], 'mod base':color_list[5], 'mod seq': 'g', 'have mod seq': 'y'}

# exporting to R
freq_table.to_csv('emoji_freq_table.csv', sep=',', encoding='utf-8')


fig, ax = plt.subplots(figsize=(6, 9))
freq_table_pop.plot(kind = 'barh', y = 'freq', x = 'name with emoji',
                    #title = 'Number of occurences for 50 most frequently used emoji'.upper(),
                    color=[colors[i] for i in freq_table_pop['standard']],
                    width = 0.5,
                    ax = ax)
#ax.set_facecolor(color='grey')
ax.set_ylabel('Emoji', fontsize = 13)
ax.set_xlabel('Number of occurences in millions', fontsize = 13)
patches, labels = ax.get_legend_handles_labels()
labels = ['simple', 'base']
patch_base = mpatches.Patch(color=color_list[5], label='Modifiable')
patch_simple = mpatches.Patch(color=color_list[2], label='Unmodifiable')
plt.legend(handles=[patch_base, patch_simple], fontsize=13, loc=4)


#for (y, x), label in zip(enumerate(freq_table_pop['freq']), list(freq_table_pop['emoji'])):
#    plt.annotate(label, xy=(x, y), va='center', fontname='Segoe UI Emoji', fontsize = 8)

fig.subplots_adjust(left=0.52)
ax.invert_yaxis()

for label in ax.get_yticklabels():
    label.set_fontproperties('Segoe UI Emoji')
    label.set_fontsize(13)

for label in ax.get_xticklabels():
    label.set_fontproperties('Segoe UI Emoji')
    label.set_fontsize(13)


ax.get_xaxis().get_major_formatter().set_scientific(False)
plt.xticks([0, 20000000, 40000000, 60000000, 80000000, 100000000 ], [0, 20, 40, 60, 80, 100])
plt.xticks(rotation=25)

plt.savefig('1_Emoji_frequency', bbox_inches='tight')


# EMOJI KOT SLIKA
#for (x, y), label in zip(enumerate(freq_table_pop['freq']), list(freq_table_pop['emoji'])):
#    plt.annotate(str(x), xy=(x, y), va='center') 
#    ab = AnnotationBbox(imagebox,[2, 1.55],
#                    xybox=(120., 100.),
#                    xycoords='data',
#                    boxcoords="offset points",
#                    pad=0.5
#                    )
#
#ax.add_artist(ab)

###############################################################################################

##############################################################################################
# aux. function: get lang dict {lang name: iso code}
def get_lang_dict(iso_codes_column):
    codes_list = list(iso_codes_column) # get iso lang codes
    final_list = [code for code in codes_list if code not in ('und', 'in', 'iw', 'chr', 'ckb', 'NA')]
    lang_dict = {code:pycountry.languages.get(alpha_2=(code.strip())).name.split(' (')[0] for code in final_list} # dict {code: get lang name for code}
    lang_dict.update({'und': 'undefined','in':'Indonesian','iw':'Hebrew','ckb':'Central kurdish', 'chr':'Cherokee', 'NA':'missing'}) # manually add unofficial codes
    return lang_dict



##############################################################################################
####### Tweet frequency by language

def get_tweet_freq_by_lang():  
    # get freq for all tweets
    all_tweets_lang = fetchall("""SELECT lang, SUM(tweet_freq)
                               FROM tweet_count
                               WHERE tweet_all IS TRUE
                               GROUP BY lang""")
    # get freq for tweets including emojis
    emoji_tweets_lang = fetchall("""SELECT lang, SUM(tweet_freq)
                                       FROM tweet_count
                                       WHERE tweet_all IS FALSE
                                       GROUP BY lang""")
    df1 = pd.DataFrame(all_tweets_lang, columns=['lang','all tweets freq'])
    df2 = pd.DataFrame(emoji_tweets_lang, columns=['lang','emoji tweets freq'])
    tweet_ratio = pd.merge(df1, df2, on='lang', how='outer')
    # add percentage of emoji tweets
    tweet_ratio['percent'] = tweet_ratio['emoji tweets freq']/tweet_ratio['all tweets freq']*100
    # sort
    tweet_ratio = tweet_ratio.sort_values(by=['percent'], ascending=False)
    # add column of language names
    lang_dict = get_lang_dict(tweet_ratio['lang'])
    tweet_ratio['lang name'] = list(map(lang_dict.get, tweet_ratio['lang']))
    
    # adding data frames by all tweets and emoji tweets
    df1['lang name'] = list(map(lang_dict.get, tweet_ratio['lang']))
    df1 = df1.sort_values(by=['all tweets freq'], ascending=False)

    df2['lang name'] = list(map(lang_dict.get, tweet_ratio['lang']))
    df2 = df2.sort_values(by=['emoji tweets freq'], ascending=False)

    #return [tweet_ratio, df1, df2]
    return tweet_ratio


lang_freq_table = get_tweet_freq_by_lang()
lang_freq_table = lang_freq_table.rename(columns = {'percent':'percent of emoji tweets'})
lang_freq_table_sorted = lang_freq_table.sort_values(by = 'all tweets freq', ascending=False)
lang_freq_table_sorted['percent of all tweets'] = lang_freq_table_sorted['all tweets freq'] / lang_freq_table_sorted['all tweets freq'].sum() * 100
lang_freq_table_sorted['non-emoji tweets freq'] = lang_freq_table_sorted['all tweets freq'] - lang_freq_table_sorted['emoji tweets freq']

# REDUCE TABLE - ONLY LANGUAGES WITH >= NUMBER OF TWEETS
lang_freq_table_pop = lang_freq_table_sorted.loc[lang_freq_table_sorted['all tweets freq'] >= 1500000]
lang_analysed = list(lang_freq_table_pop['lang name'])

    

#################### PLOT 2:  PLOTTING NUMBER OF TWEETS PER LANGUAGE #############################################


fig, ax = plt.subplots(figsize=(6, 8))
sns.set(font_scale=1.4, font='Segoe UI Emoji')

lang_freq_table_pop.set_index('lang name')[['non-emoji tweets freq','emoji tweets freq']].plot.barh(
                    #title = 'Number of tweets per language (1mio tweets not included)'.upper(),
                    color=[color_list[3], color_list[5]], legend=False,
                    ax = ax,
                    width = 0.5,
                    stacked=True)
ax.set_ylabel('Language', fontsize = 14)
ax.set_xlabel('Number of tweets in millions', fontsize = 14)


patch_emojis = mpatches.Patch(color=color_list[5], label='Tweets containing emojis')
patch_non_emojis = mpatches.Patch(color=color_list[3], label='Emoji-free tweets')
plt.legend(handles=[patch_emojis, patch_non_emojis], fontsize=14, loc=4)

plt.show()




#perc = list(lang_freq_table_sorted['percent of all tweets'])
#number_of_bars = lang_freq_table_pop.shape[0]
#p1 = ax.patches[:int((len(ax.patches)+1)/2)]
#p2 = ax.patches[int((len(ax.patches)+1)/2):]
# set individual bar lables using above list
#for i in range(0,number_of_bars):
#    # get_width pulls left or right; get_y pushes up or down
#    print(str(p1[i]))
#    ax.text(p1[i].get_width() + p1[i].get_width(), p1[i].get_y(), \
#            str(round(perc[i], 2))+'%', fontsize=8,
#                color='dimgrey')

ax.invert_yaxis()

#ax.set_xlim(0, 290996000)
ax.get_xaxis().get_major_formatter().set_scientific(False)
plt.xticks([0, 100000000, 200000000, 300000000, 400000000, 500000000, 600000000, 700000000], [0, 100, 200, 300, 400, 500, 600, 700])
plt.xticks(rotation=25)
fig.subplots_adjust(left=0.32)
fig.subplots_adjust(bottom=0.18)

plt.savefig('2_Tweet_freq_by_lang', bbox_inches='tight')

################################################################################################



##############################################################################################
######## tweet frequency by day

def get_tweet_volume():
    tweet_volume = fetchall(""" SELECT date, SUM(tweet_freq) 
                                 FROM tweet_count 
                                 WHERE tweet_all IS TRUE
                                 GROUP BY date
                                 ORDER BY date""")
    
    retweet_volume = fetchall(""" SELECT date, SUM(tweet_freq) 
                                 FROM tweet_count 
                                 WHERE tweet_all IS TRUE
                                 AND retweet IS TRUE
                                 GROUP BY date
                                 ORDER BY date""")
    
    tweet_with_emoji_volume = fetchall(""" SELECT date, SUM(tweet_freq) 
                                 FROM tweet_count 
                                 WHERE tweet_all IS FALSE
                                 GROUP BY date
                                 ORDER BY date""")

    retweet_with_emoji_volume = fetchall(""" SELECT date, SUM(tweet_freq) 
                                 FROM tweet_count 
                                 WHERE tweet_all IS FALSE
                                 AND retweet IS TRUE
                                 GROUP BY date
                                 ORDER BY date""")

    
    
    tweet_volume_df = pd.DataFrame(tweet_volume, columns = ['date', 'tweet freq'])
    tweet_with_emoji_volume_df = pd.DataFrame(tweet_with_emoji_volume, columns = ['date', 'tweet with emoji freq'])
    retweet_volume_df = pd.DataFrame(retweet_volume, columns = ['date', 'retweet freq'])
    retweet_with_emoji_volume_df = pd.DataFrame(retweet_with_emoji_volume, columns = ['date', 'retweet with emoji freq'])

    # merge all of the dataframes
    dfs = [tweet_volume_df, tweet_with_emoji_volume_df, retweet_volume_df, retweet_with_emoji_volume_df]
    volume_df = reduce(lambda left, right: pd.merge(left, right, on='date', how='outer'), dfs)

    return volume_df
    
tweet_volume_table = get_tweet_volume()


#################### PLOT 3: PLOTTING TWEET VOLUME ##################################################

fig, ax = plt.subplots(figsize=(10, 7))
tweet_volume_table.set_index('date')[['tweet freq', 'tweet with emoji freq', 'retweet freq', 'retweet with emoji freq']].plot(
                                    title = 'Number of tweets per day'.upper(),
                                    style=['-','--','-','--'],
                                    color=[color_list[3],color_list[3],color_list[5],color_list[5]], 
                                    legend=True,
                                    ax = ax)
ax.set_ylabel('Number of tweets')
ax.set_xlabel('Date')
ax.legend(['All tweets', 'Tweets containing emoji', 'All retweets', 'Retweets containing emoji'], loc=1);
plt.xticks(rotation=25)

plt.savefig('*Tweet-retweet volume')

###### emoji/all tweet ratio (tweet vs. retweet) PLOT 3b 

tweet_volume_table['emoji tweet ratio'] = tweet_volume_table['tweet with emoji freq']/tweet_volume_table['tweet freq']
tweet_volume_table['emoji retweet ratio'] = tweet_volume_table['retweet with emoji freq']/tweet_volume_table['retweet freq']

fig, ax = plt.subplots(figsize=(10, 7))
tweet_volume_table.set_index('date')[['emoji tweet ratio', 'emoji retweet ratio']].plot(
                                    title = 'Tweets including emojis/all tweets ratio per day'.upper(),
                                    #style=['-','--','-','--'],
                                    color=[color_list[3], color_list[5]], 
                                    legend=True,
                                    ax = ax)
ax.set_ylabel('Tweets including emojis/all tweets ratio')
ax.set_xlabel('Date')
ax.legend(['Tweets', 'Retweets'], loc=1);
plt.xticks(rotation=25)

#plt.savefig('*Emoji_vs_all_tweets_ratio')


##############################################################################################

# aux. function: get emoji (codes) for first n most used emojis that can be modified
def get_most_popular_codes(n): # n = number of most popular
    # find emojis which can have a modifier
    names = list(emoji_to_name.values())
    emoji_names_modifiers = [name for name in names if ':' in name and 'keycap' not in name]
    
    multi_code_freq_table = pickle.load(open(saving_dir + "multi_code_freq_table.p", "rb")) 
    multi_code_freq_table = multi_code_freq_table[multi_code_freq_table['name'].isin(emoji_names_modifiers)] # remove the ones that are not with modifiers
    
    mod_most_popular = multi_code_freq_table[0:n]
    
    codes_mod = list(map(emoji_to_code.get, mod_most_popular['emoji']))
    names_simple = [name.split(':')[0] for name in list(mod_most_popular['name'])]
    emojis_simple = list(map(name_to_emoji.get, names_simple))
    codes_simple = list(map(emoji_to_code.get, emojis_simple))
    
    return [codes_simple, codes_mod]




# table date: freq for specific simple emoji: freq for its modified version
def get_modifier_emoji_freq_in_time(code_simple, code_mod):
    # get freq for one emoji by time
    simple_df = pd.DataFrame(fetchall("""SELECT date(time_stamp), SUM(emoji_freq)
                                          FROM emoji_count
                                          WHERE emoji_id = '%s'  
                                          GROUP BY date(time_stamp)
                                          ORDER BY date(time_stamp)""" 
                                          %(code_simple)), columns=['timestamp','freq_simple'])
                    
    mod_df = pd.DataFrame(fetchall("""SELECT date(time_stamp), SUM(emoji_freq)
                                       FROM emoji_count
                                       WHERE emoji_id = '%s'  
                                       GROUP BY date(time_stamp)
                                       ORDER BY date(time_stamp)""" 
                                       %(code_mod)), columns=['timestamp','freq_mod'])
    

    all_df = pd.merge(simple_df, mod_df, on='timestamp', how='outer')
    all_df = all_df.fillna(0)
    
    return all_df

# table date: freq for specific simple emoji
def get_simple_emoji_freq_in_time(code):
    df = pd.DataFrame(fetchall("""SELECT date(time_stamp), SUM(emoji_freq)
                                  FROM emoji_count
                                  WHERE emoji_id = '%s'  
                                  GROUP BY date(time_stamp)
                                  ORDER BY date(time_stamp)""" 
                                  %(code)), columns=['timestamp','emoji_freq'])
            
    return df


def get_all_emoji_freq_in_time():
    
    df = pd.DataFrame(fetchall("""SELECT date(time_stamp), SUM(emoji_freq)
                                  FROM emoji_count
                                  GROUP BY date(time_stamp)
                                  ORDER BY date(time_stamp)""" 
                                  ), columns=['date','emoji freq'])
            
    return df


emoji_volume_table = get_all_emoji_freq_in_time()


#################### PLOT 4: PLOTTING EMOJI VOLUME ##################################################

emoji_volume_table = pd.merge(emoji_volume_table, tweet_volume_table, how='outer', on=['date'])

fig, ax = plt.subplots(figsize=(10, 7))
emoji_volume_table.set_index('date')[['emoji freq', 'tweet freq']].plot(
                                    title = 'Number of emojis per day (compared to number of tweets)'.upper(),
                                    style=['-','-'],
                                    color=[color_list[3], color_list[5]], 
                                    legend=True,
                                    ax = ax)
ax.set_ylabel('Number of tweets')
ax.set_xlabel('Date')
ax.legend(['Number of emojis', 'Number of tweets'], loc=1);
plt.xticks(rotation=25)

#plt.savefig('*Emoji_tweet_volume')



##########

emoji_volume_table.mean()
emoji_volume_table.sum()

############## SAVING DF FOR MODIFIED EMOJIS (comparison table: TIMESTAMP, SIMPLE EMOJI FREQ, MODIFIED EMOJI FREQ)

missing_names = freq_table.loc[freq_table.freq == 0].name # name of all the missing emojis
n = 1091 - len(missing_names) # 1091 is the number of all the modified emojis  
mod_dir = '/Users/miha/Documents/Emoji/emoji-analysis/Modifiers/'


# function for saving comparison (simple vs modified) table to disk - for later use
#def save_comparison_tables(n, mod_dir):
#    codes = get_most_popular_codes(n) # get codes for n most popular emoji modifiers
#    emojis = [list(map(code_to_emoji.get, code_list)) for code_list in codes]
#    names = [list(map(emoji_to_name.get, emoji_list)) for emoji_list in emojis]
#    codes_simple, codes_mod = codes[0], codes[1]
#    names_simple, names_mod = names[0], names[1]
#
#    ##    
#    first_appeared = [] # list for date that modified emoji first appeared
#    for i in range(n):
#        df_tmp = get_modifier_emoji_freq_in_time(codes_simple[i], codes_mod[i])
#        # checking where modifiers starts
#        df_tmp_nonzero_mod = df_tmp.loc[(df_tmp.freq_mod > 0)]
#    
#        # changing column names for specific emoji
#        df_tmp.columns = ['timestamp', names_simple[i], names_mod[i]]
#        df_tmp_nonzero_mod.columns = ['timestamp', names_simple[i], names_mod[i]]
#    
#        first_appeared.append(df_tmp_nonzero_mod.iloc[0])
#        
#        # save df
#        #pickle.dump(df_tmp, open(mod_dir + str(i) + "_" + names_mod[i].replace(": ", "-").replace(" ", "_") + ".p", "wb")) # save table
#    
#        
#    #pickle.dump(first_appeared, open(mod_dir + "first_appeared.p", "wb"))
#
#
## save_comparison_tables(n, mod_dir)

first_appeared = pickle.load(open(mod_dir + "first_appeared.p", "rb"))

def create_first_appeared_dict(first_appeared):
    first_appeared_dict = OrderedDict()
    
    for i in first_appeared:
        df_tmp = i.to_frame()
        date, name = df_tmp.iloc[0,0], df_tmp.index[2] # date, full name (modifier)
        first_appeared_dict[name] = date
    return first_appeared_dict
    

first_appeared = create_first_appeared_dict(first_appeared) 
    


################################################### DATES ##############################################################
#all_timestamps = pd.DataFrame(fetchall("""SELECT DISTINCT time_stamp FROM emoji_count ORDER BY time_stamp"""))
all_dates = pd.DataFrame(fetchall("""SELECT DISTINCT time_stamp::date FROM emoji_count ORDER BY time_stamp"""))
all_dates = all_dates[0].values.tolist() # list of dates
########################################################################################################################

####### PLOTTING COMPARISON TABLES
m = 0 # from m-th to n-th most popular emoji
n = 1

def get_emoji_name_from_file_name(file_name):
    tmp = file_name.split(".")[0]
    tmp = " ".join(tmp.split("_")[1:])
    return tmp.replace("-", ": ", 1)
    

# get dates for which you want to plot the comparison
os.chdir(mod_dir)
#windows
#comparison_table_names = sorted(os.listdir(mod_dir),  key=os.path.getmtime)[:-1] # get file names, sorted by creating date

#mac
comparison_table_names = os.listdir(mod_dir)
comparison_table_names.remove('first_appeared.p') # deleting first_appeared.p file form the list

def sort_modifier_files(file_list):
    tmp = [[int(file.split('_', 1)[0]), file] for file in file_list]    
    tmp_sorted = sorted(tmp, key = itemgetter(0))
    files_sorted = [tmp_list[1] for tmp_list in tmp_sorted]
    return files_sorted

comparison_table_names = sort_modifier_files(comparison_table_names)


# get dates of first appearance for n most frequently used emoji
first_dates = [first_appeared[get_emoji_name_from_file_name(file_name)] for file_name in comparison_table_names[m:n]]
first_dates.sort()
first_plotting_date = first_dates[0]

first_plotting_date = all_dates[0]
last_plotting_date = all_dates[len(all_dates)-1]

def plot_comparison(m, n, first_plotting_date, last_plotting_date, emoji_volume_table): # tweet_volume_table
    
    ax = plt.gca()
#    tweet_volume = tweet_volume_table.loc[(tweet_volume_table.date >= first_plotting_date) & (tweet_volume_table.date <= last_plotting_date)]
#    emoji_volume = emoji_volume_table.loc[(emoji_volume_table.date >= first_plotting_date) & (emoji_volume_table.date <= last_plotting_date)]

#    plt.subplot(311)
#    plt.plot('date', 'tweet freq', data=tweet_volume, linewidth=1.5, color='black')
#    plt.legend(bbox_to_anchor=(0.8, 0.8))   
#    plt.plot('date', 'tweet with emoji freq', data=tweet_volume, linewidth=1.5, color='red')
#    plt.legend(bbox_to_anchor=(0.8, 0.8))   
    
#    plt.subplot(211)

    
#    plt.plot('date', 'emoji freq', data=emoji_volume, linewidth=1.5, color='blue')
#    plt.legend(bbox_to_anchor=(0.5, 0.5))   

#    plt.subplot(212)    

    for i in comparison_table_names[m:n]:
        one_comparison = pickle.load(open(mod_dir + i, "rb")) # load emoji length dictionary
        
        # reduce to the specified dates
        one_comparison = one_comparison.loc[(one_comparison.timestamp >= first_plotting_date) & (one_comparison.timestamp <= last_plotting_date)]
    
        colnames = list(one_comparison)
    
        color = next(ax._get_lines.prop_cycler)['color']
        plt.plot('timestamp', colnames[1], data=one_comparison, linewidth = 1, color=color)
        plt.plot('timestamp', colnames[2], data=one_comparison, linestyle='dashed', linewidth = 1, color=color)
        plt.legend(loc=1)#bbox_to_anchor=(0.8, 0.8)
    


plot_comparison(m, n, first_plotting_date, last_plotting_date, emoji_volume_table) # tweet_volume_table


####################################################################################
# number of different emoji used by language / TIME CONSUMING !!!

#def get_number_of_different_emojis():
#    diff_emoji_freq_table = pd.DataFrame(fetchall("""SELECT lang, COUNT(lang)
#                                                     FROM (SELECT DISTINCT lang, emoji_id 
#                                                     FROM emoji_count) 
#                                                     AS distinct_lang_emoji
#                                                     GROUP BY lang"""),
#                                        columns=['lang','diff emoji freq'])
#
#    diff_emoji_freq_table = diff_emoji_freq_table.sort_values(by=['diff emoji freq'], ascending=False) # sort
#    lang_dict = get_lang_dict(diff_emoji_freq_table['lang']) # get lang dict
#    diff_emoji_freq_table['lang name'] = list(map(lang_dict.get, diff_emoji_freq_table['lang'])) # add lang name
#    return diff_emoji_freq_table
#
#diff_emoji_freq_table = get_number_of_different_emojis()

# SAVING DF
#pickle.dump(diff_emoji_freq_table, open(saving_dir + "diff_emoji_freq_table.p", "wb")) # save table

# number of different modifier emoji used by language
def get_number_of_different_mod_emojis():
    diff_emoji_mod_freq_table = pd.DataFrame(fetchall("""SELECT DISTINCT lang, emoji_count.emoji_id, name
                                                     FROM emoji_count LEFT JOIN emoji ON emoji_count.emoji_id = emoji.emoji_id
                                                     WHERE name LIKE '%skin tone%'
                                                     """),
                                        columns=['lang','id','name'])
    diff_emoji_mod_freq_table['mod base'] = diff_emoji_mod_freq_table['name'].apply(lambda x: x.split(':')[0])
    # create table with number of different modified emojis
    tmp_table = diff_emoji_mod_freq_table[['lang', 'mod base']] # get only relevant columns
    #####
    # check which are missing
    english_mod = tmp_table[tmp_table['lang'] == 'en']['mod base']
    slo_mod = tmp_table[tmp_table['lang'] == 'sl']['mod base']
    all_mod = set([name.split(':')[0] for name in name_to_emoji.keys() if 'skin tone' in name])
    all_mod - set(english_mod)
    all_mod - set(slo_mod)
    #####
    
    tmp_table = tmp_table.drop_duplicates() # remove duplicates
    tmp_table = tmp_table.groupby(['lang']).agg(['count']) # count by language
    tmp_table.columns = tmp_table.columns.droplevel()
    tmp_table = tmp_table.reset_index()
    tmp_table = tmp_table.sort_values(by=['count'], ascending=False) # sort
    lang_dict = get_lang_dict(tmp_table['lang']) # get lang dict
    tmp_table['lang name'] = list(map(lang_dict.get, tmp_table['lang'])) # add lang name\
    tmp_table = tmp_table.rename(columns={'count': 'mod count'})
    
    return tmp_table


# number of different gender emoji used by language - EITHER MAN OR WOMAN
def get_man_to_woman_dict():
    man_to_woman = {}
    with open('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/Gender_analysed.csv') as gender_mod:
        next(gender_mod)
        for line in gender_mod:
            tmp = line.strip().split(';')
            man_to_woman[tmp[1]] = tmp[2]
            man_to_woman[tmp[2]] = tmp[2] # quick fix: adding woman:woman
    return man_to_woman
    
def get_number_of_different_gender_emojis():
    diff_emoji_gender_freq_table = pd.DataFrame(fetchall("""SELECT DISTINCT lang, emoji_count.emoji_id, name
                                                     FROM emoji_count LEFT JOIN emoji ON emoji_count.emoji_id = emoji.emoji_id
                                                     WHERE gender_comparison = 'Man' OR gender_comparison = 'Woman' OR gender_comparison = 'Woman Single' OR gender_comparison = 'Man Single'
                                                     """),
                                        columns=['lang','id','name'])
    diff_emoji_gender_freq_table['gender'] = diff_emoji_gender_freq_table['name'].apply(lambda x: x.split(':')[0])

    # create table with number of different gender emojis
    diff_emoji_gender_freq_table = diff_emoji_gender_freq_table.replace({'# ':''}, regex=True)
    tmp_table = diff_emoji_gender_freq_table[['lang', 'gender']] # get only relevant columns
    
    man_to_woman = get_man_to_woman_dict()
    pd.options.mode.chained_assignment = None

    tmp_table['gender'] = tmp_table.gender.map(man_to_woman)
    
    tmp_table = tmp_table.drop_duplicates() # remove duplicates
    tmp_table = tmp_table.groupby(['lang']).agg(['count']) # count by language

    tmp_table.columns = tmp_table.columns.droplevel()
    tmp_table = tmp_table.reset_index()
    tmp_table = tmp_table.sort_values(by=['count'], ascending=False) # sort
    lang_dict = get_lang_dict(tmp_table['lang']) # get lang dict
    
    tmp_table['lang name'] = list(map(lang_dict.get, tmp_table['lang'])) # add lang name
    tmp_table = tmp_table.rename(columns={'count': 'gender count'})
    
    return tmp_table

diff_emoji_mod_freq_table = get_number_of_different_mod_emojis()
diff_emoji_gender_freq_table = get_number_of_different_gender_emojis()



############### PLOT 5: PLOTTING NUMBER OF DIFFERENT EMOJIS PER LANGUAGE #####################

diff_emoji_freq_table = pickle.load(open(saving_dir + "/diff_emoji_freq_table.p", "rb"))
# correcting names .. modern greek (...)
diff_emoji_freq_table['lang name'] = diff_emoji_freq_table['lang name'].apply(lambda x:x.split(' (')[0])

# merging with NUMBER OF DIFFERENT MODIFIER EMOJI FREQ TABLE
diff_emoji_freq_table = pd.merge(diff_emoji_mod_freq_table[['lang name', 'mod count']], diff_emoji_freq_table, how='right', on = 'lang name')
# merging with NUMBER OF DIFFERENT GENDER EMOJI FREQ TABLE
diff_emoji_freq_table = pd.merge(diff_emoji_gender_freq_table[['lang name', 'gender count']], diff_emoji_freq_table, how='right', on = 'lang name')

# adding 'others'
diff_emoji_freq_table['others'] = diff_emoji_freq_table['diff emoji freq'] - diff_emoji_freq_table['mod count'] - diff_emoji_freq_table['gender count']

# get only languages from lang_analysed
diff_emoji_freq_table_pop = diff_emoji_freq_table[diff_emoji_freq_table['lang name'].isin(lang_analysed)]


color_list = sns.cubehelix_palette(8,light=1)
### Sort by language analysed
sns.set(font_scale=1.4, font='Segoe UI Emoji')
# Create the dictionary that defines the order for sorting
sorterIndex = dict(zip(lang_analysed,range(len(lang_analysed))))
# Generate a rank column that will be used to sort the dataframe numerically
diff_emoji_freq_table_pop['lang_sort'] = diff_emoji_freq_table_pop['lang name'].map(sorterIndex)
diff_emoji_freq_table_pop = diff_emoji_freq_table_pop.sort_values('lang_sort')

fig, ax = plt.subplots(figsize=(6, 8))
diff_emoji_freq_table_pop.set_index('lang name')[['diff emoji freq']].plot.barh(
                    #title = 'Number of different emoji used per language'.upper(),
                    color=[color_list[3]], legend=False,
                    ax = ax,
                    width = 0.5)

ax.set_ylabel('Language', fontsize = 15)
ax.set_xlabel('Number of different emojis', fontsize = 15)

for (y, x), label in zip(enumerate(diff_emoji_freq_table_pop['diff emoji freq']), list(diff_emoji_freq_table_pop['diff emoji freq'])):
    plt.annotate(label, xy=(x, y), va='center', fontname='Segoe UI Emoji', fontsize = 11)


plt.show()

ax.invert_yaxis()
plt.xticks(rotation=25)
fig.subplots_adjust(left=0.3)
ax.set_xlim(0, 2920)


plt.savefig('3_Diff_emojis_per_lang', bbox_inches='tight')



########### 5b

# TO INTEGER
diff_emoji_freq_table_pop['mod count'] = diff_emoji_freq_table_pop['mod count'].astype('int')
diff_emoji_freq_table_pop['gender count'] = diff_emoji_freq_table_pop['gender count'].astype('int')

### Sort by language analysed

sns.set(font_scale=1.4, font='Segoe UI Emoji')
# Create the dictionary that defines the order for sorting
sorterIndex = dict(zip(lang_analysed,range(len(lang_analysed))))
# Generate a rank column that will be used to sort the dataframe numerically
diff_emoji_freq_table_pop['lang_sort'] = diff_emoji_freq_table_pop['lang name'].map(sorterIndex)
diff_emoji_freq_table_pop = diff_emoji_freq_table_pop.sort_values('lang_sort')

#fig, ax = plt.subplots(figsize=(6, 8))
fig, (ax, ax2) = plt.subplots(figsize=(6, 8), ncols=2, sharey=True)
diff_emoji_freq_table_pop.set_index('lang name')[['mod count']].plot.barh(
                    #title = 'Number of different emoji used per language'.upper(),
                    color=[color_list[5]], legend=False,
                    ax = ax,
                    width = 0.5)

diff_emoji_freq_table_pop.set_index('lang name')[['gender count']].plot.barh(
                    #title = 'Number of different emoji used per language'.upper(),
                    color=[color_list[5]], legend=False,
                    ax = ax2,
                    width = 0.5)

ax.set_ylabel('Language', fontsize = 15)
ax.set_xlabel('No. of different \n skin tone modified \n emojis', fontsize = 13)
ax2.set_xlabel('No. of different \n  gender specific \n emojis', fontsize = 13)


for (y, x), label in zip(enumerate(diff_emoji_freq_table_pop['mod count']), list(diff_emoji_freq_table_pop['mod count'])):
    ax.annotate(label, xy=(x, y), va='center', fontname='Segoe UI Emoji', fontsize = 11)

for (y, x), label in zip(enumerate(diff_emoji_freq_table_pop['gender count']), list(diff_emoji_freq_table_pop['gender count'])):
    print((x,y))
    ax2.annotate(label, xy=(x, y), va='center', fontname='Segoe UI Emoji', fontsize = 11)


plt.show()

ax.invert_yaxis()
#ax.yaxis.tick_right()
fig.subplots_adjust(left=0.3)
fig.subplots_adjust(bottom=0.2)
ax.set_xlim(0, 252)
ax2.set_xlim(0, 52)

plt.savefig('3_b_Diff_emojis_per_lang', bbox_inches='tight')







####################################################################################
# INDEX
#os.chdir('/Users/miha/Documents/Emoji/emoji-analysis/')
#from create_tables import execute_query
#execute_query("""CREATE INDEX emoji_id ON emoji_count (emoji_id)""")
#execute_query("""CREATE INDEX lang ON emoji_count (lang)""")

####################################################################################


################### HEATPLOT : LANGUAGE - SKIN TONE / GENDER ###################################### 

def plot_heatmap(df_tmp, final_columns, pivot_columns, figsize, xlabel, title,
                 add_sum, sum_column, to_sum_columns, saving_name, xticklabels,
                 left_adjustment, x_ticks_size, y_ticks_size, x_title_size,
                 y_title_size, annot_size, bottom_adjustment):

    lang_dict = get_lang_dict(df_tmp['lang']) # get language dict {lang code:lang name}
    df_tmp['lang name'] = list(map(lang_dict.get, df_tmp['lang'])) # add language names
    df_tmp= df_tmp.pivot(index='lang name', columns=pivot_columns, values='count') # rearrange the table
    df = df_tmp.fillna(0) # NaN to zeros
    
    # sort languages by number of tweets
    df['lang name'] = df.index # copy index column to another column for merging
    df_merged = pd.merge(lang_freq_table_sorted[['lang name', 'all tweets freq']], df, how='right', on = 'lang name') # merge with number of tweets table
    df_sorted = df_merged.sort_values(by=['all tweets freq'], ascending=False) # sort descending by number of tweets
    df_sorted.index = df_sorted['lang name'] # add lang names back to index (rownames)
    
    if add_sum:
        df_sorted[sum_column] = df_sorted[to_sum_columns].sum(axis=1)

    df_sorted['abs freq'] = df_sorted[final_columns].sum(axis=1) # add absolute final row frequency
    
    tmp = final_columns + ['abs freq']
    # get percentage
    df_plot = df_sorted[tmp].copy()
    df_plot[final_columns] = df_plot[final_columns].div(df_plot[final_columns].sum(axis=1), axis=0) # convert to percent - multyiply by 100 automatically with heatmap
    df_plot = df_plot.ix[lang_analysed] # REDUCER BY lang_anaylsed
    
    # ADD NUMBER OF EMOJIS ANALYSED TO LANG NAMES
    new_index = []
    for index, row in df_plot.iterrows():
        freq = int(row['abs freq'])
        new_index.append(index + ' (' + str(freq) + ')') #+ str(round(freq/1000000,2)) + ')')
    
    df_plot.index = new_index

   
    # heatmap
    sns.set(font_scale=1.4, font='Segoe UI Emoji')
    cmap = sns.cubehelix_palette(8, as_cmap=True)
    fig, ax = plt.subplots(1,1,figsize=figsize) # sample figsize in inches
    sns.heatmap(df_plot[final_columns], annot=True, ax=ax, fmt='.0%', cmap=cmap, annot_kws={"size": annot_size})
    cbar = ax.collections[0].colorbar
    
    cbar.set_ticks([0.25,0.5,0.75])
    cbar.set_ticklabels(['25%', '50%', '75%'])
    fig.subplots_adjust(left=left_adjustment)
    if bottom_adjustment:
        fig.subplots_adjust(left=bottom_adjustment)


    ax.set_ylabel('Language (number of emojis analysed)', fontsize=y_title_size)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize = x_title_size)
    ax.set_xticklabels(xticklabels, fontsize = x_ticks_size)
    ax.set_yticklabels(new_index, fontsize = y_ticks_size)

    if final_columns == ['light', 'medium-light', 'medium', 'medium-dark', 'dark']:
        plt.xticks(rotation=31, ha='right')
    
    plt.savefig(saving_name, bbox_inches='tight')
    return


######### PLOT 6: LANGUAGE - SKIN TONE

# PLOT 6a COMPARE BASE vs SKIN TONE IN GENERAL
query = """SELECT lang, skin_tone, SUM(emoji_freq)
            FROM emoji_count AS ec LEFT JOIN emoji AS e ON ec.emoji_id=e.emoji_id
            WHERE skin_tone IS NOT NULL AND time_stamp::date >= '2015-03-18'
            GROUP BY lang, skin_tone
            ORDER BY lang, skin_tone
            """
df_base_st = pd.DataFrame(fetchall(query), columns=['lang','skin_tone','count']) # from db to pandas df
                         
final_columns = ['base', 'skin tone']
pivot_columns = 'skin_tone'
sum_column = 'skin tone'
to_sum_columns = ['light', 'medium-light', 'medium', 'medium-dark', 'dark']
xlabel = None
xticklabels = ['Base', 'Modified']
title = 'Skin tone usage per language'
saving_name = '4_Base_skin_tone'

figsize = (5.5,8)
left_adjustment = 0.41
bottom_adjustment = None
x_ticks_size = 11
y_ticks_size = 12
x_title_size = 12
y_title_size = 12
annot_size = 11

add_sum = True

plot_heatmap(df_base_st, final_columns, pivot_columns, figsize, xlabel, title,
             add_sum, sum_column, to_sum_columns, saving_name, xticklabels, 
             left_adjustment, x_ticks_size, y_ticks_size, x_title_size, y_title_size,
             annot_size, bottom_adjustment)

##### PAZI (POPRAVI???): pri zaokrozavanju pri racunanju procentov pride pri Uighur jeziku 0% za skin tone
# http://unicode.org/reports/tr51/#Emoji_ZWJ_Sequences

##################### PLOT 6b COMPARE skin tones

query = """SELECT lang, skin_tone, SUM(emoji_freq)
            FROM emoji_count AS ec LEFT JOIN emoji AS e ON ec.emoji_id=e.emoji_id
            WHERE skin_tone IS NOT NULL AND time_stamp::date >= '2015-03-18'
            GROUP BY lang, skin_tone
            ORDER BY lang, skin_tone"""
df_st = pd.DataFrame(fetchall(query), columns=['lang','skin_tone','count']) # from db to pandas df
                          
final_columns = ['light', 'medium-light', 'medium', 'medium-dark', 'dark']
pivot_columns = 'skin_tone'
xlabel = 'Skin tone types'
xticklabels = ['Light', 'Medium-light', 'Medium', 'Medium-dark', 'Dark']
title = 'Skin tone usage per language'
add_sum = False
saving_name = '5_Skin_tones'

figsize = (6,9)
left_adjustment = 0.37
bottom_adjustment = None
x_ticks_size = 11
y_ticks_size = 13
x_title_size = 13
y_title_size = 13
annot_size = 11

plot_heatmap(df_st, final_columns, pivot_columns, figsize, xlabel, title,
             add_sum, None, None, saving_name, xticklabels, left_adjustment,
             x_ticks_size, y_ticks_size, x_title_size, y_title_size, annot_size,
             bottom_adjustment)

#########################################################################################


###################### PLOT 7: LANGUAGE - GENDER 


# PLOT 7a COMPARE Neutral vs. Gender defined
query = """SELECT lang, gender_comparison, SUM(emoji_freq)
            FROM emoji_count AS ec LEFT JOIN emoji AS e ON ec.emoji_id=e.emoji_id
            WHERE (gender_comparison = 'Man' OR gender_comparison = 'Woman' OR gender_comparison = 'Neutral')
                AND time_stamp::date >= '2016-09-13'
            GROUP BY lang, gender_comparison
            ORDER BY lang, gender_comparison"""

df_gender = pd.DataFrame(fetchall(query), columns=['lang','gender','count']) # from db to pandas df

final_columns = ['Neutral', 'Man', 'Woman']
pivot_columns = 'gender'
#sum_column = 'Gender defined' # name of the added sum column
#to_sum_columns = ['Woman', 'Man']
xlabel = 'Gender'
xticklabels = ['Neutral', 'Male', 'Female']
title = 'Gender usage per language'
add_sum = False
saving_name = '7_Gender'

figsize = (5.3,9)
left_adjustment = 0.43
bottom_adjustment = None
x_ticks_size = 11
y_ticks_size = 12
x_title_size = 12
y_title_size = 12
annot_size = 11

plot_heatmap(df_gender, final_columns, pivot_columns, figsize, xlabel, title,
             add_sum, None, None, saving_name, xticklabels, 
             left_adjustment, x_ticks_size, y_ticks_size, x_title_size, y_title_size,
             annot_size, bottom_adjustment)


# PLOT 7b COMPARE Man vs. Woman
#query = """SELECT lang, gender_comparison, COUNT(*)
#            FROM emoji_count AS ec LEFT JOIN emoji AS e ON ec.emoji_id=e.emoji_id
#            WHERE gender_comparison = 'Man' OR gender_comparison = 'Woman' OR gender_comparison = 'Neutral'
#            GROUP BY lang, gender_comparison
#            ORDER BY lang, gender_comparison"""
#
#df_tmp = pd.DataFrame(fetchall(query), columns=df_columns) # from db to pandas df
#df_columns = ['lang','gender','count']
#final_columns = ['Woman', 'Man']
#pivot_columns = 'gender'
#figsize = (5,9)
#xlabel = 'Gender'
#xticklabels = final_columns
#title = 'Gender usage per language'
#add_sum = True
#saving_name = '6_Neutral_Gender'
#
#plot_heatmap(query, df_columns, final_columns, pivot_columns, figsize, xlabel,
#             title, False, None, None, saving_name, xticklabels)



# check which emojis are analyzed here = which has Woman or Man or Neutral as gender_comparison
emoji_gender_comparison = pd.DataFrame(fetchall("""SELECT name, gender_comparison FROM emoji 
                                    WHERE gender_comparison = 'Man' OR gender_comparison = 'Woman' OR gender_comparison = 'Neutral'"""),
                                    columns=['name', 'gender'])





########################### STACKED AREA PLOT ################################

###################### PLOT 8: DATE - SKIN TONE

query = """SELECT time_stamp::date, skin_tone, SUM(emoji_freq)
           FROM emoji_count ec LEFT JOIN emoji e ON ec.emoji_id = e.emoji_id
           WHERE skin_tone IS NOT NULL
           GROUP BY time_stamp::date, skin_tone"""    
    
df_original_skin_tone = pd.DataFrame(fetchall(query), columns=['date', 'skin tone', 'count']) # from db to pandas df


def get_skin_tone_stacked_plot(df_original_skin_tone, without_base, group_by, N, saving_name):
    
    # define colors     
    skin_tone_color_list = [[255/255, 204/255, 34/255], [255/255,219/255,172/255], [241/255,194/255,125/255],
                            [224/255,172/255,105/255],[198/255,134/255,66/255], [141/255,85/255,36/255]]
    
    # rearange table
    df = df_original_skin_tone.pivot(index='date', columns='skin tone', values='count') # rearrange the table
    df = df.fillna(0) # NaN to zeros

    if without_base:
        # remove base and reorder
        df = df[['light', 'medium-light', 'medium', 'medium-dark', 'dark']]
        skin_tone_color_list = skin_tone_color_list[1:]
    else: # reorder
        df = df[['base', 'light', 'medium-light', 'medium', 'medium-dark', 'dark']]

    # group by n days
    if group_by:
        dates = list(df.index)
        df_by_n = df.reset_index()
        df_by_n = df_by_n.groupby(df_by_n.index//N).sum()
        dates_by_n = dates[::N]
        df_by_n.index = dates_by_n
        df = df_by_n
        
    # get %
    df_perc = df.div(df.sum(axis=1), axis=0) # convert to percent - multyiply by 100 automatically with heatmap
    
    df_perc = df_perc.fillna(0) # NaN to zeros (NaN = 0/0 when no skin tone type)

    # rename - capitalize
    df_perc.columns = [column_name.capitalize() for column_name in df_perc.columns] # rename columns

    # plot area
    fig, ax = plt.subplots(figsize=(9, 2.8)) # 9,6
    df_perc[df_perc.columns[::-1]].plot.area(color=list(reversed(skin_tone_color_list)), alpha=1, ax=ax, linewidth=0) # 
    plt.xticks(rotation=25)
    fig.subplots_adjust(bottom=0.24)
    #fig.subplots_adjust(left=0.23)
    
    legend_indices = [5,4,3,2,1,0]
    if without_base:
        legend_indices = [4,1,3,0,2] # poglej ce je treba popravit vrstni red!!!
    
    handles, labels = ax.get_legend_handles_labels()
    #handles_tmp = [handles[i] for i in legend_indices]
    labels_tmp = [labels[i] for i in legend_indices] 
    legend_elements = [Patch(facecolor=skin_tone_color_list[legend_indices[5]], edgecolor='black',
                         label=labels_tmp[0]),
                       Patch(facecolor=skin_tone_color_list[legend_indices[4]], edgecolor='black',
                         label=labels_tmp[1]),
                       Patch(facecolor=skin_tone_color_list[legend_indices[3]], edgecolor='black',
                         label=labels_tmp[2]),
                       Patch(facecolor=skin_tone_color_list[legend_indices[2]], edgecolor='black',
                         label=labels_tmp[3]),
                       Patch(facecolor=skin_tone_color_list[legend_indices[1]], edgecolor='black',
                         label=labels_tmp[4]),
                       Patch(facecolor=skin_tone_color_list[legend_indices[0]], edgecolor='black',
                         label=labels_tmp[5])]
    
    
    ax.legend(loc = 'lower center', bbox_to_anchor = (0.13, 0.25), fontsize = 9, ncol = 1, # 18
              handles = legend_elements)
    ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y))) 
    ax.set_xlabel('Date', fontsize = 11) # 18
    
    ax.set_xlim(pd.Timestamp('2014-09-12'), pd.Timestamp('2017-12-18'))
    ax.set_ylim(0,1)
    
    # fontsize ticklabels
    for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(9)  # 16
    for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(9)  # 16
            
        
    plt.savefig(saving_name)#, bbox_inches='tight')
    return


N = 7
sns.set(font='Segoe UI Emoji')

# 1 ALL
# all dates
get_skin_tone_stacked_plot(df_original_skin_tone, False, False, N, '6_Skin_tone_dates_all' )
# group by N dates
get_skin_tone_stacked_plot(df_original_skin_tone, False, True, N, '6_Skin_tone_dates_all_7days' )

# 2 SKIN TONE ONLY
# all dates
#get_skin_tone_stacked_plot(df_original_skin_tone, True, False, N, '6_Skin_tone_dates')
# group by N dates
#get_skin_tone_stacked_plot(df_original_skin_tone, True, True, N, '6_Skin_tone_dates_7days')



###################### PLOT 9: DATE - GENDER

query = """SELECT time_stamp::date, gender_comparison, SUM(emoji_freq)
           FROM emoji_count ec LEFT JOIN emoji e ON ec.emoji_id = e.emoji_id
           WHERE gender_comparison IS NOT NULL AND gender_comparison != 'Neutral Multi'
           GROUP BY time_stamp::date, gender_comparison"""    

df_original_gender = pd.DataFrame(fetchall(query), columns=['date', 'gender', 'count']) # from db to pandas df


def get_gender_stacked_plot(df_original_gender, not_included, columns_ordered, column_names,
                            color_list, bbox_to_anchor, saving_name, group_by, N):
    
    # remove values
    if not_included:
        df_tmp = df_original_gender[~df_original_gender['gender'].isin(not_included)]
    else:
        df_tmp = df_original_gender
        
    # rearange table
    df = df_tmp.pivot(index='date', columns='gender', values='count')
    df = df.fillna(0) # NaN to zeros

    #df[(df['Man'] > 0)]
    #df[(df['Woman'] > 0)]

    # group by n days
    if group_by:
        dates = list(df.index)
        df_by_n = df.reset_index()
        df_by_n = df_by_n.groupby(df_by_n.index//N).sum()
        dates_by_n = dates[::N]
        df_by_n.index = dates_by_n
        df = df_by_n

    # get %
    df_perc = df.div(df.sum(axis=1), axis=0) # convert to percent - multyiply by 100 automatically with heatmap
    df_perc = df_perc[columns_ordered] # reorder columns
    
    # rename
    df_perc.columns = column_names
    
    # plot area
    sns.set(font='Segoe UI Emoji')
    
    fig, ax = plt.subplots(figsize=(9, 2.8)) # 9, 6
    df_perc[df_perc.columns[::-1]].plot.area(color=color_list, alpha=1, ax=ax, linewidth=0)
    plt.xticks(rotation=25)
    fig.subplots_adjust(bottom=0.24)
    #fig.subplots_adjust(left=0.28)    

    legend_indices = [5,2,4,1,3,0]
    if not_included:
        legend_indices = [2,1,0]
    
    handles, labels = ax.get_legend_handles_labels()
    handles_tmp = [handles[i] for i in legend_indices]
    labels_tmp = [labels[i] for i in legend_indices]
    legend_elements = [Patch(facecolor=color_list[legend_indices[0]], edgecolor='black',
                         label=labels_tmp[0]),
                       Patch(facecolor=color_list[legend_indices[1]], edgecolor='black',
                         label=labels_tmp[1]),
                       Patch(facecolor=color_list[legend_indices[2]], edgecolor='black',
                         label=labels_tmp[2]),
                       Patch(facecolor=color_list[legend_indices[3]], edgecolor='black',
                         label=labels_tmp[3]),
                       Patch(facecolor=color_list[legend_indices[4]], edgecolor='black',
                         label=labels_tmp[4]),
                       Patch(facecolor=color_list[legend_indices[5]], edgecolor='black',
                         label=labels_tmp[5])]


    legend = ax.legend(loc = 'lower center', bbox_to_anchor = bbox_to_anchor, fontsize = 9, ncol = 1, #(0.5, -0.68)
              handles = legend_elements)
    ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y))) 
    ax.set_xlabel('Date', fontsize = 11)
    
    ax.set_xlim(pd.Timestamp('2014-09-12'), pd.Timestamp('2017-12-18'))
    ax.set_ylim(0,1)

     # fontsize ticklabels
    for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(9) 
    for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(9) 
            
    plt.savefig(saving_name) #, bbox_inches='tight')
    return

N = 7
color_list_n = [[173/255, 154/255, 175/255], [233/255, 227/255, 234/255]]
color_list_w = [[229/255, 111/255, 99/255], [243/255, 173/255, 166/255]]
color_list_m = [[119/255, 168/255, 199/255], [169/255, 216/255, 246/255]]
   

 
# 1  WITHOUT SINGLES  - popravi legendo 
#not_included = ['Man Single', 'Woman Single', 'Neutral Single']
#columns_ordered = ['Neutral', 'Man', 'Woman']
#color_list = [color_list_w[0], color_list_m[0], color_list_n[0]]
#bbox_to_anchor = (-0.35, 0.65)
#column_names = ['Neutral', 'Male', 'Female']
#saving_name = '8_Gender_dates_combined_7days'
#get_gender_stacked_plot(df_original_gender, not_included, columns_ordered, column_names,
#                        color_list, bbox_to_anchor, saving_name, True, N)
# 
#saving_name = '8_Gender_dates_combined'
#get_gender_stacked_plot(df_original_gender, not_included, columns_ordered, column_names,
#                        color_list, bbox_to_anchor, saving_name, False, N)
#   
# 2 WITH SINGLES = ALL
not_included = None
columns_ordered = ['Neutral', 'Man', 'Woman', 'Neutral Single', 'Man Single', 'Woman Single']
color_list = [color_list_w[0], color_list_m[0], color_list_n[0], color_list_w[1], color_list_m[1], color_list_n[1]]
bbox_to_anchor = (0.13, 0.25)
column_names = ['Neutral', 'Male', 'Female', 'Neutral (SCP)', 'Male (SCP)', 'Female (SCP)']
saving_name = '8_Gender_dates_all_7days'
get_gender_stacked_plot(df_original_gender, not_included, columns_ordered, column_names,
                        color_list, bbox_to_anchor, saving_name, True, N)

saving_name = '8_Gender_dates_all'
get_gender_stacked_plot(df_original_gender, not_included, columns_ordered, column_names, 
                        color_list, bbox_to_anchor, saving_name, False, N)

# 3 ONLY SINGLES
#not_included = ['Man', 'Woman', 'Neutral']
#columns_ordered = ['Neutral Single', 'Man Single', 'Woman Single']
#color_list = [color_list_w[1], color_list_m[1], color_list_n[1]]
#bbox_to_anchor = (-0.42, 0.65)
#column_names = ['Neutral (SCP)', 'Male (SCP)', 'Female (SCP)']
#saving_name = '8_Gender_dates_singles_7days'
#get_gender_stacked_plot(df_original_gender, not_included, columns_ordered, column_names,
#                        color_list, bbox_to_anchor, saving_name, True, N)
#
#saving_name = '8_Gender_dates_singles'
#get_gender_stacked_plot(df_original_gender, not_included, columns_ordered, column_names,
#                        color_list, bbox_to_anchor, saving_name, False, N)






### Additional

################ sexual orientation (holding hands)

query = """SELECT lang, real_gender, COUNT(*)
                            FROM emoji_count AS ec LEFT JOIN emoji AS e ON ec.emoji_id=e.emoji_id
                            WHERE real_gender = 'MM' OR real_gender = 'MF' OR real_gender = 'FF'
                            GROUP BY lang, real_gender
                            ORDER BY lang, real_gender
                            """
                            
df_columns = ['lang','gender','count']
final_columns = ['FF', 'MF', 'MM']
pivot_columns = 'gender'
figsize = (8,9)
xlabel = 'Gender Pair'
title = 'Gender pair comparison for holding hands emoji'

plot_heatmap(query, df_columns, final_columns, pivot_columns, figsize, xlabel, title, False, None, None)


################### Santa Claus

query = """SELECT lang, name, COUNT(*)
            FROM emoji_count AS ec LEFT JOIN emoji AS e ON ec.emoji_id=e.emoji_id
            WHERE name = 'Santa Claus' OR name = 'Mrs. Claus'
            GROUP BY lang, name
            ORDER BY lang, name
            """
df_columns = ['lang','name','count']                          
final_columns = ['Santa Claus', 'Mrs. Claus']
pivot_columns = 'name'
figsize = (8,9)
xlabel = 'Gender'
title = 'Santa Claus gender comparison'

plot_heatmap(query, df_columns, final_columns, pivot_columns, figsize, xlabel, title, False, None, None)

 
################     


###### General info (# od tweets with/without emojis())
    
tweet_count = psql.read_sql("""SELECT * FROM tweet_count""", conn)
all_tweet_count = tweet_count[tweet_count.tweet_all == True]
emoji_tweet_count = tweet_count[tweet_count.tweet_all == False]

tweet_volume_table.mean()
tweet_volume_table.sum()

# languages
lang_freq_table_sorted
 
import numpy as np
# % of tweets including emojis per language
lang_emoji_perc = lang_freq_table_sorted
lang_emoji_perc['emoji perc by lang'] = lang_freq_table_sorted[['emoji tweets freq']]/lang_freq_table_sorted[['emoji tweets freq']].sum()*100
lang_emoji_perc['greater than 1'] = np.where(lang_emoji_perc['emoji perc by lang']>=1, 'yes', 'no')

greater = lang_emoji_perc[lang_emoji_perc['greater than 1'] == 'yes'][['lang name', 'emoji perc by lang']]
others = lang_emoji_perc[lang_emoji_perc['emoji perc by lang'] < 1][['lang name', 'emoji perc by lang', 'greater than 1']]
others[['emoji perc by lang']].sum()
others['lang name']
len(others['lang name'])

tmp = ['other', float(others[['emoji perc by lang']].sum())]
greater.loc[len(greater)] = tmp
greater.sort_values(by = ['emoji perc by lang'], ascending = False)
###### Emoji info

number_of_all_emojis = fetchone("""SELECT COUNT(emoji_id) FROM emoji""")[0]
types = pd.DataFrame(fetchall("""SELECT modified, COUNT(*) FROM emoji GROUP BY modified"""), columns=['type','freq'])
skin_tone = fetchone("""SELECT COUNT(skin_tone) FROM emoji WHERE skin_tone = 'base'""")[0]
gender = fetchone("""SELECT COUNT(skin_tone) FROM emoji WHERE gender IS NOT NULL""")[0]





       
