# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 16:34:00 2017

@author: Lena
"""

# INSERT TO TABLES

import psycopg2
import pickle
import os
#import ujson
import re
from collections import Counter

#os.chdir('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\emoji-analysis')
os.chdir('/Users/miha/Documents/Emoji/emoji-analysis')

from config import config


#directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\Dictionaries\\'
directory = '/Users/miha/Documents/Emoji/emoji-analysis/Dictionaries/'
emoji_to_code = pickle.load(open(directory + 'emoji_to_code.p', "rb")) 
emoji_to_name = pickle.load(open(directory + 'emoji_to_name.p', "rb"))
code_to_name = pickle.load(open(directory + 'code_to_name.p', "rb"))
name_to_length = pickle.load(open(directory + "name_to_length.p", "rb"))


# create gender dict
def get_gender_dict(is_code): # if code IS TRUE {code:name}, if code IS FALSE {name:gender}
    gender_dict = {}
    with open('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/gender_mod.txt') as gender_mod:
        for line in gender_mod:
            tmp = line.split(' ',1)
            gender = tmp[0]
            tmp2 = tmp[1].split(' ',1)
            code, name = tmp2[0], tmp2[1]
            if is_code:
                gender_dict[code] = name.strip()
            else:
                gender_dict[name.strip()] = gender
    return gender_dict


def get_skin_tone_dict():
    skin_tone_dict = {}
    with open('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/skin_tone_mod.txt') as skin_tone_mod:
        for line in skin_tone_mod:
            tmp = line.split('\t',1)
            code, name = tmp[0], tmp[1]
            skin_tone_dict[code] = name.replace('skin tone', '').strip()
    return skin_tone_dict


def get_flags_dict():
    flags_dict = {}
    with open('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/flags.txt') as flags:
        for line in flags:
            tmp = line.split(' ',1)
            emoji, name = tmp[0], tmp[1]
            code = emoji_to_code[emoji]
            flags_dict[code] = name.strip()
    return flags_dict


def get_modifier_info():
    mod_info_dict = dict.fromkeys(code_to_name.keys())
    singles = [code for code in code_to_name.keys() if len(code.split(' ')) == 1]
    multi = [code.split(' ') for code in code_to_name.keys() if len(code.split(' ')) > 1]
    
    for code in singles: # for each single coded
        if any(code in x for x in multi): # check if any of it appears in multi coded
            mod_info_dict[code] = 'base'
        else:
            mod_info_dict[code] = 'simple'
    
    for code in multi: # for each multi coded
        mod = [s for s in singles if s in code]
        if mod: # check if any of single coded appears in it
            mod_info_dict[' '.join(code)] = 'modifier'
        else:
            mod_info_dict[' '.join(code)] = 'multi'
    
    return mod_info_dict
    

def get_skin_tone_base():
    st_base = list(set([name.split(':')[0] for name in code_to_name.values() if 'skin tone' in name]))
    return st_base
    
def get_standard_info():
    emoji_mod_base = []
    emoji_mod_seq = []
    with open('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/emoji_modifier_sequence.txt') as standard:
            for line in standard:
                tmp = line.split(';',1)[0].split(' ')[0:2]
                tmp = ['U+' + s for s in tmp]
                emoji_mod_base.append(tmp[0])
                emoji_mod_seq.append(' '.join(tmp))
    return [emoji_mod_seq, emoji_mod_base]

#################### gender specification ###########
import pandas as pd 

#def none_to_string(s):
#    return 'New emoji' if s is None else s
#
#gender_data = pd.DataFrame([])
#with open('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/person_list') as person_list:
#    for group in person_list:
#        tmp = group.split(':')
#        group_name, emojis = tmp[0], tmp[1].split(' ')
#        
#        for emoji in emojis:
#            emoji = emoji.strip()
#            gender_data = gender_data.append(pd.DataFrame({'Emoji': emoji, 'Code': none_to_string(emoji_to_code.get(emoji)), 'Name': none_to_string(emoji_to_name.get(emoji)), 'Group': group_name}, index=[0]), ignore_index=True)
  
#gender_data.to_csv('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/gender_data', sep='\t', encoding='utf-8')
# manually corrected gender data: added Neutral/Man/Woman and how Neutral looks like



def get_gender_data_comparison():
    gender_data_cor = pd.read_csv('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/gender_data.csv', sep = '\t', index_col=False) # gender standard
    del gender_data_cor['Unnamed: 0']
    del gender_data_cor['Emoji']
    gender_data_cor = gender_data_cor.where(pd.notnull(gender_data_cor), None) # NaN to None
#    gender_data_cor = gender_data_cor[pd.notnull(gender_data_cor['Comparison'])]
#    null_comparison = gender_data_cor[gender_data_cor['Comparison'].isnull()]
    
    # adding skin tone gender versions
    gender_data_cor_st = pd.DataFrame([])
    for index, row in gender_data_cor.iterrows():
        # append base
        gender_data_cor_st = gender_data_cor_st.append(pd.DataFrame({'Code': row['Code'], 'Group': row['Group'], 'Name': row['Name'], 'Comparison': row['Comparison'], 'Neutral looks like': row['Neutral looks like']}, index=[0]), ignore_index=True)
        name = row['Name']
        st_names = [name + st for st in [': light skin tone', ': medium-light skin tone', ': medium skin tone', ': medium-dark skin tone', ': dark skin tone'] ]
        for st in st_names: # for each skin tone 
            code = name_to_code.get(st)
#            if not code:
#                print(name)
            if code: # zombie, genie,... without skin tones
                gender_data_cor_st = gender_data_cor_st.append(pd.DataFrame({'Code': code, 'Group': row['Group'], 'Name': st, 'Comparison': row['Comparison'], 'Neutral looks like': row['Neutral looks like']}, index=[0]), ignore_index=True)
     
    # for checking
    #gender_data_cor_st.to_csv('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/gender_data_skin_tone.csv', sep='\t', encoding='utf-8')
    return gender_data_cor_st   


####################################################


def insert_emoji():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor()
    
    # compile regex for finding all gender codes
    gender_dict = get_gender_dict(is_code=True)
    real_gender_dict = get_gender_dict(is_code=False)

    r_gender = re.compile(u'(' + u'|'.join(re.escape(code) for code in gender_dict.keys()) + u')')

    # compile regex for finding all skin tone codes
    skin_tone_base = get_skin_tone_base()
    skin_tone_dict = get_skin_tone_dict()
    r_skin_tone = re.compile(u'(' + u'|'.join(re.escape(code) for code in skin_tone_dict.keys()) + u')')

    # compile regex for finding all flags code
    flags_dict = get_flags_dict()
    r_flags = re.compile(u'(' + u'|'.join(re.escape(code) for code in flags_dict.keys()) + u')')

    # get modifier info (simple, multi, modifier, base)
    mod_info_dict = get_modifier_info()

    # GET STANDARD INFO (modifier sequence, modifier base and including modifier sequence)
    [emoji_mod_seq, emoji_mod_base] = get_standard_info() # skin tone standard
    
    gender_data_cor_st = get_gender_data_comparison()
    
    code_to_comparison = dict(zip(gender_data_cor_st['Code'], gender_data_cor_st['Comparison']))
    code_to_lookslike = dict(zip(gender_data_cor_st['Code'], gender_data_cor_st['Neutral looks like']))
    code_to_group = dict(zip(gender_data_cor_st['Code'], gender_data_cor_st['Group']))
        
    for (code, name) in code_to_name.items():
        # get number of code points
        length = name_to_length[name]
        
        # get modifier info
        mod = mod_info_dict[code]
            
        # get skin tone
        skin_tone = r_skin_tone.findall(code)
        if skin_tone:
            skin_tone = skin_tone_dict[skin_tone[0]]
        else:
            if name in skin_tone_base:
                skin_tone = 'base'
            else:
                skin_tone = None
                  
        # get gender part of emoji
        gender = r_gender.findall(code)
        combined_gender = None
        combined_gender_type = None
        gender_names = None
        real_gender = None
        standard = None
        gender_comparison = None

        combined_all_sorted = ['woman', 'man', 'girl', 'boy'] # list for sorting combined gender
        if gender:
            gender_names = list(map(gender_dict.get, gender))

            # get combined gender info
            if len(gender) > 1:            
                gender_names_sorted = sorted(gender_names, key=lambda x: combined_all_sorted.index(x))
                combined_gender = ', '.join(gender_names_sorted) # join all appeared gender to one string
                combined_gender_type = name.split(':')[0] # 3 types: kiss, couple with heart or family
                gender_names = 'combined' # if there are more than 1 gender, we set it to 'combine'
                real_gender = 'C'
                
            else:
                gender_names = gender_names[0]
                real_gender = real_gender_dict[gender_names]
        
        # STANDARD GENDER INFO - person group, gender comparison (Neutral-Man-Woman) and Netural looks like
        gender_comparison = code_to_comparison.get(code)
        neutral_looks_like = code_to_lookslike.get(code)
        person_group = code_to_group.get(code)
        
        # get flag
        flag = r_flags.findall(code)
        if flag:
            flag = flags_dict[flag[0]]
        else:
            flag = None

        # get other
        other = None
        if not gender and not skin_tone and not flag and name_to_length[name] > 1:
            other = name
           
        variation_selector, zwj = False, False    
            
        # variation selector
        if 'U+FE0F' in code:
            variation_selector = True
        
        # zwj
        if 'U+200D' in code:
            zwj = True
          
        # STANDARD: emoji modified sequence...
        if code in emoji_mod_seq: # modifier sequence
            standard = 'mod seq'
        elif any(ms in code for ms in emoji_mod_seq): # includes modifier sequence
            standard = 'have mod seq'
        elif code in emoji_mod_base: # base
            standard = 'mod base'
            
            
        # insert to db   
        cur.execute("""INSERT INTO emoji(emoji_id, name, length, modified, standard, skin_tone,
                                         gender_comparison, netural_looks_like, person_group, 
                                         gender, real_gender, combined_gender, combined_gender_type,
                                         flags, other_multicoded, variation_selector, zwj)
                       VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", 
                       (code, name, length, mod, standard, skin_tone, gender_comparison, 
                        neutral_looks_like, person_group, gender_names, real_gender, 
                        combined_gender, combined_gender_type, flag, other, 
                        variation_selector, zwj))
        
    conn.commit()
    cur.close()
    conn.close()
    print("Emoji table inserted")
    return


####################################################################################################

global data_directory
data_directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\CountsData\\'

def get_timestamp(file_name):
    tmp = file_name.replace('-emojis', '').rpartition('-')
    timestamp = ' '.join([tmp[0], tmp[2]])
    return ''.join([timestamp,':00:00'])
    
def get_bool_retweet(retweet_info):
    return True if retweet_info == 'T' else False
        


def insert_folder(folder):  
    files = os.listdir(data_directory + folder)
    for file in files:
        insert_file(file)
    print("Folder %s inserted." % folder)
    return


def insert_file(file):
    with open(data_directory + folder + '\\' + file, encoding = 'utf-8') as file_hour: # open file
        json_count = ujson.load(file_hour) # load json
        for emoji, counts in json_count.items(): # for each emoji
            if counts: # if emoji has info
                for lang, freq_dict in counts.items(): # for each language
                    for retweet_info, freq in freq_dict.items(): # for each frequency (T or F)
                        try:
                            cur.execute("INSERT INTO emoji_count(time_stamp, emoji_id, lang, retweet, emoji_freq) VALUES (%s, %s, %s, %s, %s)",
                                    (get_timestamp(file), emoji_to_code[emoji], lang, get_bool_retweet(retweet_info), freq))
                        except Exception as error:
                            print(error)
    conn.commit()
    return                    
                        
                        
 
   

def insert_emoji_count():
    params = config()
    
    global conn
    conn = psycopg2.connect(**params)
    
    global cur
    cur = conn.cursor()
    
    
    folders = os.listdir(data_directory) # list all folders
    #folders = folders[0:20]
    del folders[-1] # delete GeneralCounter for tweet counts
    
    global folder
    for folder in folders:
        insert_folder(folder)
    
       
    cur.close()
    conn.close()
    print('Last folder inserted: %s' % folder)




##################################################################################################

def get_bool_all_vs_emoji(all_vs_emoji):
    return True if all_vs_emoji == 'All' else False

# data_directory + 'GeneralCounter'
def insert_tweet_count():
    params = config()
    conn = psycopg2.connect(**params)
    cur = conn.cursor()
    
    with open('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\CountsData\\GeneralCounter', encoding = 'utf-8') as counter_file:
        next(counter_file)
        for line in counter_file:
            tmp = line.split('\t')
            date, json_count = tmp[0], ujson.loads(tmp[1].strip())
            for all_vs_emoji, counts in json_count.items(): # for ALL or EMOJI
                 for lang, freq_dict in counts.items(): # for each language
                    for retweet_info, freq in freq_dict.items(): # for each frequency (T or F)
                        try:
                            cur.execute("INSERT INTO tweet_count(date, lang, retweet, tweet_all, tweet_freq) VALUES (%s, %s, %s, %s, %s)",
                                    (date, lang, get_bool_retweet(retweet_info), get_bool_all_vs_emoji(all_vs_emoji) , freq))
                        except Exception as error:
                            print(error)
    conn.commit()   
    cur.close()
    conn.close()

##################################################################################################

#insert_emoji()
#insert_emoji_count()
#insert_tweet_count()

 
# truncating tables 
#os.chdir('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\emoji-analysis')
os.chdir('/Users/miha/Documents/Emoji/emoji-analysis/')
from create_tables import execute_query

truncate_emoji = (""" TRUNCATE TABLE emoji """)
truncate_emoji_count = (""" TRUNCATE TABLE emoji_count """)
truncate_tweet_count = (""" TRUNCATE TABLE tweet_count """)

#execute_query(truncate_emoji)
#execute_query(truncate_tweet_count)

















