# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 14:56:50 2017

@author: Lena
"""

# CREATE TABLES

import os
#os.chdir('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\PythonFiles\\emoji-analysis')
os.chdir('/Users/miha/Documents/Emoji/emoji-analysis/')
from config import config
import psycopg2


def execute_query(command):
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(command)
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


create_emoji = (""" CREATE TABLE emoji (
                        emoji_id VARCHAR(70) PRIMARY KEY,
                        name VARCHAR(70),
                        length INT,
                        modified VARCHAR(10),
                        standard VARCHAR(30),
                        skin_tone VARCHAR(40),
                        gender_comparison VARCHAR(40),
                        netural_looks_like VARCHAR(40),
                        person_group VARCHAR(40), 
                        gender VARCHAR(40),
                        real_gender VARCHAR(2),
                        combined_gender VARCHAR(50),
                        combined_gender_type VARCHAR(50),
                        flags VARCHAR(50),
                        other_multicoded VARCHAR(50),
                        variation_selector BOOLEAN,
                        zwj BOOLEAN)""")
 

create_emoji_count = (""" CREATE TABLE emoji_count (
                        emoji_count_id BIGSERIAL PRIMARY KEY,
                        time_stamp TIMESTAMP NOT NULL,
                        emoji_id VARCHAR(70) NOT NULL,
                        lang VARCHAR(10),
                        retweet BOOLEAN,
                        emoji_freq INT) """)



create_tweet_count = (""" CREATE TABLE tweet_count (
                        tweet_count_id BIGSERIAL PRIMARY KEY,
                        date DATE NOT NULL,                            
                        lang VARCHAR(10),
                        retweet BOOLEAN,
                        tweet_all BOOLEAN,
                        tweet_freq INT) """)
 

drop_emoji = (""" DROP TABLE emoji """)
drop_emoji_count = (""" DROP TABLE emoji_count """)
drop_tweet_count = (""" DROP TABLE tweet_count """)

##########################################################


# CREATE TABLES
#execute_query(create_emoji)
#execute_query(create_tweet_count)



# DROP TABLES
#execute_query(drop_emoji)
#execute_query(drop_tweet_count)














