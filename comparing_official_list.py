#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 12:21:56 2018

@author: Lena
"""

import pickle


# comparing official list (from 24. 10. 2017) with updated official list (2. 3. 2018)

code_to_name_updated = {}
emoji_to_name_updated = {}
name_to_emoji_updated = {}

with open ('/Users/miha/Documents/Emoji/emoji-analysis/Official_Emoji_List/unicode-updated.txt') as updated_list:
    for line in updated_list:
        tmp = line.split('\t')
        code, emoji, name = tmp[1], tmp[2], tmp[len(tmp)-1].strip().replace('# ','')
        code_to_name_updated [code] = name 
        emoji_to_name_updated [emoji] = name
        name_to_emoji_updated [name] = emoji
      

dict_dir = '/Users/miha/Documents/Emoji/emoji-analysis/Dictionaries/'
name_to_emoji = pickle.load(open(dict_dir + 'name_to_emoji.p', "rb")) # load emoji official dictionary
name_to_emoji = dict([ (name.replace('# ', ''), emoji) for name, emoji in name_to_emoji.items( ) ])



def not_in_list(list1,list2):
    dict1 = dict(zip(list1,list1))
    returnList2 = [x for x in list2 if x not in dict1]
    return returnList2


new = not_in_list(name_to_emoji, name_to_emoji_updated) # emoji names in updated - NEW EMOJIS
removed = not_in_list(name_to_emoji_updated, name_to_emoji) # removed emojis

# checking if only skin tone is removed
removed_others = [name for name in removed if 'skin tone' not in name]
removed_skin_tone = list(set([name.split(':')[0] for name in removed if 'skin tone' in name]))

len(removed_skin_tone) # 212 is correct number of emojis with skin modifier option

# school backpack and white medium star

saving_dir = '/Users/miha/Documents/Emoji/emoji-analysis/DataFrames' # mac
freq_table = pickle.load(open(saving_dir + "freq_table.p", "rb"))


freq_table.loc[freq_table['name'] == 'school backpack'] 
freq_table.loc[freq_table['name'] == 'white medium star']

##### CONCLUSION:
# 1.  SCHOOL BACKPACK changed to BACKPACK 
# 2.  WHITE MEDIUM STAR changed to star
# 3.  EMOJIS WITH SKIN TONE MODIFIERS REMOVED ONLY FROM TABLE

