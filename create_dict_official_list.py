# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 14:55:07 2017

@author: Lena
"""

# In this file we create a dictionary from official emoji list {emoji:name}
# and another dictionary for emoji length {name:length}

import collections
import grapheme
import pickle
import os

os.chdir('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis')
directory = 'C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\SavedVar\\'


# create dict {emoji: name} from official list
emoji_official = collections.OrderedDict()
emoji_length = collections.OrderedDict()

with open('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\FilesForCreatingOfficialList\\only_unicode.csv', encoding='utf-8') as unicode:
    next(unicode) # skip header
    i = 0
    for line in unicode: # for each line = code point, name
        i = i + 1
        line = line.split(",",1)
        code, name = line[0].replace('"', ''),line[1].replace('"', '').strip() # remove \n + remove quotes
        temp = code.replace('U+','') # remove U+ 
        temp_list = str.split(temp) # seperate multichar in a list
        emoji_list = [chr(int(x,16)) for x in temp_list]
        emoji_string = ''.join(emoji_list)
        emoji_official[emoji_string] = name
        emoji_length[name] = len(code.split(" "))
        if grapheme.length(emoji_string) != 1: break

pickle.dump(emoji_official, open(directory + "emoji_official.p", "wb")) # save emoji official dictionary
pickle.dump(emoji_length, open(directory + "emoji_length.p", "wb")) # save emoji length by name


####################################################################################################3
# create new dict {emoji:unicode official}


emoji_official = pickle.load(open(directory + 'emoji_official.p', "rb")) # load emoji official dictionary
emoji_length = pickle.load(open(directory + "emoji_length.p", "rb")) # load emoji dict for length

inverse_emoji_official = {v: k for k, v in emoji_official.items()}

emoji_code = collections.OrderedDict()
with open('C:\\Users\\Administrator\\Desktop\\Lena\\Emojis\\FilesForCreatingOfficialList\\only_unicode.csv', encoding='utf-8') as unicode_file:
    next(unicode_file)
    for line in unicode_file:
        tmp = line.split(',',1)
        code, name = tmp[0].replace('"', ''), tmp[1].strip().replace('"', '')
        emoji = inverse_emoji_official[name]
        emoji_code[emoji] = code
      
pickle.dump(emoji_code, open(directory + "emoji_code.p", "wb"))


